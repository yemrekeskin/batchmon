﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SysMon.Models
{
    public interface IModel
        : IModel<int>
    {

    }

    public interface IModel<T>
    {
        T Id { get; set; }
    }
}
