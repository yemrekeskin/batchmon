﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SysMon.Models
{
    public enum ActivePassive
    {
        Passive = 0,
        Active = 1
    }
}
