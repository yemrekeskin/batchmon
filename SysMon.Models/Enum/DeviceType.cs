﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SysMon.Models
{
    public enum DeviceType
    {
        IOS = 1,
        ANDROID = 2
    }
}
