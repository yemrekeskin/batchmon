﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SysMon.Models
{
    public enum JobState
    {
        UnKnown = 0,
        Failed = 1,
        Success = 2,
        Running = 3
    }
}
