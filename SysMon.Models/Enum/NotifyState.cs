﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SysMon.Models
{
    public enum NotifyState
    {
        None = 1,
        Sent = 2,
        Viewed = 3,
        Canceled = 4
    }
}
