﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SysMon.Models.Enum
{
    public class Sounds
    {
        public const string Default = Siren;

        public const string Bugle = "bugle.mp3";
        public const string Rush = "rush.mp3";
        public const string Siren = "siren.mp3"; 
        public const string Ticktock = "ticktock.mp3";
        public const string Ringtone1 = "ringtone1.mp3";
        public const string Ringtone2 = "ringtone2.mp3";
        public const string Ringtone3 = "ringtone3.mp3";
        public const string Ringtone4 = "ringtone4.mp3";
        public const string Ringtone5 = "ringtone5.mp3"; 
    }
}
