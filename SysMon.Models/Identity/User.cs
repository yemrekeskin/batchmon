﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity;

namespace SysMon.Models 
{
    public class User
        : IdentityUser
    {
        public string Name { get; set; }
        public string Surname { get; set; }

        public int GroupId { get; set; }

        public string Token { get; set; }
        public string DeviceId { get; set; } 
        public DeviceType DeviceType { get; set; }
        public string Sound { get; set; }
    }
}
