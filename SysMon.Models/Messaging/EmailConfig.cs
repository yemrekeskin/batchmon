﻿namespace SysMon.Models
{
    public class EmailConfig
    {
        public string SmtpServer { get; set; }
        public int SmtpPort { get; set; }

        public string Name { get; set; }
        public string SmtpUsername { get; set; }
        public string SmtpPassword { get; set; }

        public string Subject { get; set; }

    }
}