﻿using System.Collections.Generic;

namespace SysMon.Models
{
    public class EmailMessage
    {
        public List<string> To { get; set; }

        public string Subject { get; set; }
        public string Content { get; set; }

        public EmailMessage()
        {
            this.To = new List<string>();
        }
    }
}