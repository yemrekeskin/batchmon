﻿using System.Collections.Generic;

namespace SysMon.Models
{
    public class SmsMessage
    {
        public string To { get; set; }
        public string Message { get; set; }
    }
}