﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SysMon.Models
{
    public class Config
        : BaseModel
    {
        public string GroupName { get; set; }   
        public string Key { get; set; }
        public string Value { get; set; } 
    }
}
