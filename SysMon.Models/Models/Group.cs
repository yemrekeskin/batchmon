﻿using System;

namespace SysMon.Models
{
    public class Group
        : BaseModel
    {
        public string GroupName { get; set; }
        public string Explanation { get; set; }
    }
}
