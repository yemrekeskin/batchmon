﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SysMon.Models
{
    public class Job
        : BaseModel
    {
        public int JobRunId { get; set; }

        public string JobName { get; set; }
        public string JobDetail { get; set; }
        public  JobState JobState { get; set; }

        public int Delay { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }

        public string ProjectName { get; set; }

    }
}
