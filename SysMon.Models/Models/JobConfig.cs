﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SysMon.Models
{
    public class JobConfig
        : BaseModel
    {
        public string UserId { get; set; }
        public int JobId { get; set; }

        public bool NotifyOnFailure { get; set; }
        public bool NotifyOnDelay { get; set; }
        public int ThresholdForDelay { get; set; }
        public bool Hot { get; set; }
    }
}
