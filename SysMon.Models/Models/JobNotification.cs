﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SysMon.Models
{
    public class JobNotification
        : BaseModel
    {
        public string UserId { get; set; }
        public int JobId { get; set; }

        public string Title { get; set; }
        public string Message { get; set; }
        public string Sound { get; set; }
        
        public NotifyState NotifyState { get; set; }
        public DateTime LastSentDate { get; set; }

        public int Counter { get; set; }

        public int JobRunId { get; set; }
    }
}
