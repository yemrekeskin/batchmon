﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SysMon.Models
{
    public class JobSchedule
        : BaseModel
    {
        public string UserId { get; set; }
        public DateTime DateTime  { get; set; }
    }
}
