﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SysMon.Models
{
    public class Watcher
        : BaseModel
    {
        public int GroupId { get; set; }

        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        
        public string ManagerName { get; set; }
        public string ManagerEmail { get; set; }
        public string ManagerPhone { get; set; }

        public bool IsWatcher { get; set; }
    }
}
