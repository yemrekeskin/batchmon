﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SysMon.Models
{
    public class NotificationMessage
    {
        public string DeviceId { get; set; }
        public DeviceType DeviceType { get; set; }
        public string Message { get; set; }
        public string Sound { get; set; }

    }
}
