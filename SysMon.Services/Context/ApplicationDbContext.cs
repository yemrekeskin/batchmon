﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SysMon.Models;
using Microsoft.AspNetCore.Identity;

namespace SysMon.Services
{
    public class ApplicationDbContext 
        : IdentityDbContext<User,Role,string>, IDbContext
    {

        public DbSet<Group> Groups { get; set; }
        public DbSet<Job> Jobs { get; set; }
        public DbSet<JobConfig> JobConfigs { get; set; }
        public DbSet<JobSchedule> JobSchedule { get; set; }
        public DbSet<JobNotification> JobNotifications { get; set; }
        public DbSet<Watcher> Watchers { get; set; }
        public DbSet<Config> Configs { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {

        }

        public ApplicationDbContext()
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Core Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Core Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
            //builder.Entity<IdentityUserRole<Guid>>().HasKey(p => new { p.UserId, p.RoleId });
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=213.142.145.140;Database=yemrekeskin_ApplicationDb3;Trusted_Connection=True;Integrated Security=False;MultipleActiveResultSets=true;user id=ApplicationDb3;password=Kqs1m1^4");
            base.OnConfiguring(optionsBuilder);
        }
    }
}
