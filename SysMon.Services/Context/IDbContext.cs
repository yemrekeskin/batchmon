﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace SysMon.Services
{
    public interface IDbContext
    {
        DbSet<TModel> Set<TModel>() where TModel : class;

        int SaveChanges();
    }
}
