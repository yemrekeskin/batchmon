﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace SysMon.Services
{
    public static class ApplicationBuilderExtentions
    {
        public static void ApplySocketConfiguration(this IApplicationBuilder app)
        {
            string assemblyName = "SysMon.Services";
            var assembly = Assembly.Load(new AssemblyName(assemblyName));
            foreach (var type in assembly.ExportedTypes)
            {
                if (type.BaseType == typeof(WebSocketHandler))
                {
                    var handler = app.ApplicationServices.GetService(type.BaseType);
                    app.Map("/ws", (item) => item.UseMiddleware<SocketMiddleware>(handler));
                }
            }
        }
    }
}
