﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;

namespace SysMon.Services
{
    public static class StringExtensions
    {
        public static bool IsEmailValid(this string emailaddress)
        {
            try
            {
                MailAddress m = new MailAddress(emailaddress);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }
    }
}
