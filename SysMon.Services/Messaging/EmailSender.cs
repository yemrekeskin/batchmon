﻿using SysMon.Models;
using MailKit.Net.Smtp;
using Microsoft.Extensions.Logging;
using MimeKit;
using MimeKit.Text;
using System;
using System.Linq;

namespace SysMon.Services
{
    public interface IEmailSender
    {
        bool Send(EmailMessage emailMessage);
    }

    public class EmailSender
        : IEmailSender
    {
        private readonly ILogger<EmailSender> logger;
        private readonly IConfigService configService;

        public EmailSender(
            ILogger<EmailSender> logger,
            IConfigService configService)
        {
            this.logger = logger;
            this.configService = configService;
        }

        public EmailConfig GetMailConfig()
        {
            var configs = configService.List();
            var config = new EmailConfig();

            var SmtpServer = configs.Single(d => d.Key == "SMTP_SERVER");
            var SmtpPort = configs.Single(d => d.Key == "SMTP_PORT");
            var SmtpUserName = configs.Single(d => d.Key == "SMTP_USERNAME");
            var SmtpPassword = configs.Single(d => d.Key == "SMTP_PASSWORD");
            var Subject = configs.Single(d => d.Key == "SUBJECT");

            config.SmtpServer = SmtpServer.Value;
            config.SmtpPort = int.Parse(SmtpPort.Value);
            config.SmtpUsername = SmtpUserName.Value;
            config.SmtpPassword = SmtpPassword.Value;
            config.Subject = Subject.Value;

            return config;
        }

        public bool Send(EmailMessage emailMessage)
        {
            var Succeed = true;

            var config = this.GetMailConfig();

            var message = new MimeMessage();
            message.Subject = config.Subject;
            message.Body = new TextPart(TextFormat.Html)
            {
                Text = emailMessage.Content
            };
            message.From.Add(new MailboxAddress(config.Name, config.SmtpUsername));
            message.To.AddRange(emailMessage.To.Select(d => new MailboxAddress(emailMessage.Subject, d)));

            using (var emailClient = new SmtpClient())
            {
                try
                {
                    //emailClient.Connect("smtp.gmail.com", 587);
                    //emailClient.Authenticate("msysmon34@gmail.com", "Sb615434.");
                    emailClient.Connect(config.SmtpServer, config.SmtpPort);
                    emailClient.Authenticate(config.SmtpUsername, config.SmtpPassword);
                    emailClient.Send(message);
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, ex.Message);
                    Succeed = false;
                }
                finally
                {
                    emailClient.Disconnect(true);
                }
            }
            return Succeed;
        }
    }
}