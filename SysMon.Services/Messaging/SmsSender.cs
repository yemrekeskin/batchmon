﻿using SysMon.Models;
using Microsoft.Extensions.Logging;
using System;

namespace SysMon.Services
{
    public interface ISmsSender
    {
        bool Send(SmsMessage smsMessage);
    }

    /// <summary>
    /// https://www.twilio.com/blog/2017/02/next-generation-csharp-helper-library-release.html
    /// </summary>
    public class SmsSender
        : ISmsSender
    {
        private readonly ILogger<SmsSender> logger;

        public SmsSender(
            ILogger<SmsSender> logger)
        {
            this.logger = logger;
        }

        public bool Send(SmsMessage smsMessage)
        {
            bool Succeed = true;
            var config = new SmsConfig();
            
            try
            {
                
            }
            catch (Exception ex)
            {
                logger.LogError(ex, ex.Message);
                Succeed = false;
            }
            return Succeed;
        }
    }
}