﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;

namespace SysMon.Services
{
    public class SocketMiddleware
    {
        private readonly RequestDelegate next;

        private WebSocketHandler webSocketHandler { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SocketMiddleware"/> class.
        /// </summary>
        /// <param name="next">Request delegate instance.</param>
        /// <param name="webSocketHandler">Web socket handler.</param>
        public SocketMiddleware(RequestDelegate next, WebSocketHandler webSocketHandler)
        {
            this.next = next;
            this.webSocketHandler = webSocketHandler;
        }

        /// <summary>
        /// Socket Invoke Method.
        /// </summary>
        /// <param name="context">Http context.</param>
        /// <returns>Task</returns>
        public async Task Invoke(HttpContext context)
        {
            if (!context.WebSockets.IsWebSocketRequest)
            {
                return;
            }

            if (webSocketHandler.IsExists(context))
            {
                return;
            }

            var socket = await webSocketHandler.OnConnected(context);

            await Receive(socket, async (result, buffer) =>
            {
                if (result.MessageType == WebSocketMessageType.Text)
                {
                    await webSocketHandler.ReceiveAsync(socket, result, buffer);
                    return;
                }
                else if (result.MessageType == WebSocketMessageType.Close)
                {
                    await webSocketHandler.OnDisconnected(socket);
                    return;
                }
            });
        }

        /// <summary>
        /// Message receiver.
        /// </summary>
        /// <param name="socket">Socket Instance.</param>
        /// <param name="handleMessage">Message as a byte list.</param>
        /// <returns>Task Result.</returns>
        private async Task Receive(NetWebSocket socket, Action<WebSocketReceiveResult, byte[]> handleMessage)
        {
            var buffer = new byte[1024 * 4];

            while (socket.WebSocket.State == WebSocketState.Open)
            {
                var result = await socket.WebSocket.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);

                handleMessage(result, buffer);
            }
        }
    }
}
