﻿using SysMon.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SysMon.Services
{
    public interface IConfigRepository
       : IRepository<Config>
    {

    }

    public class ConfigRepository
        : BaseRepository<Config>, IConfigRepository
    {

    }
}
