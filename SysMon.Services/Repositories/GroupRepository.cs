﻿using SysMon.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SysMon.Services
{
    public interface IGroupRepository
       : IRepository<Group>
    {

    }

    public class GroupRepository
        : BaseRepository<Group>, IGroupRepository
    {

    }
}
