﻿using SysMon.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SysMon.Services
{
    public interface IJobConfigRepository
       : IRepository<JobConfig>
    {

    }

    public class JobConfigRepository
        : BaseRepository<JobConfig>, IJobConfigRepository
    {

    }
}
