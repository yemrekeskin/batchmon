﻿using SysMon.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SysMon.Services
{
    public interface IJobNotificationRepository
      : IRepository<JobNotification>
    {

    }

    public class JobNotificationRepository
        : BaseRepository<JobNotification>, IJobNotificationRepository
    {

    }
}
