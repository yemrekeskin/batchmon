﻿using SysMon.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SysMon.Services
{
    public interface IJobRepository
       : IRepository<Job>
    {

    }

    public class JobRepository
        : BaseRepository<Job>, IJobRepository
    {

    }
}
