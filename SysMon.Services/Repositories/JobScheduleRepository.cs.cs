﻿using SysMon.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SysMon.Services
{
    public interface IJobScheduleRepository
      : IRepository<JobSchedule>
    {

    }

    public class JobScheduleRepository
        : BaseRepository<JobSchedule>, IJobScheduleRepository
    {

    }
}
