﻿using SysMon.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SysMon.Services
{
    public interface IWatcherRepository
      : IRepository<Watcher>
    {

    }

    public class WatcherRepository
        : BaseRepository<Watcher>, IWatcherRepository
    {

    }
}
