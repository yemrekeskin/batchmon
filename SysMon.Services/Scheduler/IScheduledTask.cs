﻿using System.Threading;
using System.Threading.Tasks;

namespace SysMon.Services
{
    public interface IScheduledTask
    {
        string Schedule { get; }
        void Execute(CancellationToken cancellationToken);
    }
}
