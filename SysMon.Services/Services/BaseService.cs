﻿using SysMon.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace SysMon.Services
{
    public abstract class BaseService<TModel>
        : IService<TModel>
        where TModel : BaseModel
    {
        protected readonly IRepository<TModel> repo;

        public BaseService(IRepository<TModel> repo)
        {
            this.repo = repo;
        }

        public TModel Add(TModel model)
        {
            repo.Add(model);
            return model;
        }

        public void AddBatch(ICollection<TModel> models)
        {
            repo.AddBatch(models);
        }

        public IEnumerable<TModel> FromSql(string sqlQuery, params object[] parameters)
        {
            return repo.FromSql(sqlQuery, parameters);
        }

        public TModel Get(TModel model)
        {
            return repo.Get(model.Id);
        }

        public TModel Get(long Id)
        {
            return repo.Get(Id);
        }

        public IEnumerable<TModel> List()
        {
            return repo.List();
        }

        public IEnumerable<TModel> List(IEnumerable<long> ids)
        {
            return repo.List(ids);
        }

        public IEnumerable<TModel> List(Expression<Func<TModel, bool>> predicate)
        {
            var result = repo.List(predicate);
            return result;
        }

        public void Remove(TModel model)
        {
            repo.Remove(model);
        }

        public void Remove(long Id)
        {
            repo.Remove(Id);
        }

        public void Remove(Expression<Func<TModel, bool>> predicate)
        {
            repo.Remove(predicate);
        }

        public TModel Update(TModel model)
        {
            repo.Update(model);
            return model;
        }
    }
}
