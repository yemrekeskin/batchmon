﻿using SysMon.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SysMon.Services
{
    public interface IConfigService
        : IService<Config>
    {

    }

    public class ConfigService
        : BaseService<Config>, IConfigService
    {
        public ConfigService(IConfigRepository repo)
            : base(repo)
        {

        }
    }
}
