﻿using SysMon.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SysMon.Services
{
    public interface IGroupService
        : IService<Group>
    {

    }

    public class GroupService
        : BaseService<Group>, IGroupService
    {
        public GroupService(IGroupRepository repo)
            : base(repo)
        {

        }
    }
}
