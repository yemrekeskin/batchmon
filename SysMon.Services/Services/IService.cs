﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace SysMon.Services
{
    public interface IService<TModel>
    {
        TModel Add(TModel model);
        void AddBatch(ICollection<TModel> models);
 
        TModel Update(TModel model);

        void Remove(TModel model);
        void Remove(long Id); 
        void Remove(Expression<Func<TModel, bool>> predicate);

        TModel Get(TModel model);
        TModel Get(long Id);

        IEnumerable<TModel> List();
        IEnumerable<TModel> List(IEnumerable<long> ids);
        IEnumerable<TModel> List(Expression<Func<TModel, bool>> predicate);

        IEnumerable<TModel> FromSql(string sqlQuery, params object[] parameters);
    }
}
