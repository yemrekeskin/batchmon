﻿using SysMon.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SysMon.Services
{
    public interface IJobConfigService
        : IService<JobConfig>
    {

    }

    public class JobConfigService
        : BaseService<JobConfig>, IJobConfigService
    {
        public JobConfigService(IJobConfigRepository repo)
            : base(repo)
        {

        }
    }
}
