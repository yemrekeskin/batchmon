﻿using SysMon.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SysMon.Services
{
    public interface IJobNotificationService
        : IService<JobNotification>
    {

    }

    public class JobNotificationService
        : BaseService<JobNotification>, IJobNotificationService
    {
        public JobNotificationService(IJobNotificationRepository repo)
            : base(repo)
        {

        }
    }
}
