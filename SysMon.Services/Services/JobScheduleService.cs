﻿using SysMon.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SysMon.Services
{
    public interface IJobScheduleService
        : IService<JobSchedule>
    {

    }

    public class JobScheduleService
        : BaseService<JobSchedule>, IJobScheduleService
    {
        public JobScheduleService(IJobScheduleRepository repo)
            : base(repo)
        {

        }
    }
}
