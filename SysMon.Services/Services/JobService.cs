﻿using SysMon.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SysMon.Services
{
    public interface IJobService
        : IService<Job>
    {
        List<Job> GetFailedJobs();
    }

    public class JobService
        : BaseService<Job>, IJobService
    {
        public JobService(IJobRepository repo)
            : base(repo)
        {
            
        }

        public List<Job> GetFailedJobs()
        {
            var result = base.FromSql("SELECT [Id],[JobRunId],[JobName],[JobDetail],[JobState],[Delay],[StartTime],[EndTime],[ProjectName] FROM [Jobs]", "");
            return result.ToList();
        }
    }
}
