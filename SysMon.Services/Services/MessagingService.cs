﻿using SysMon.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SysMon.Services
{
    public interface IMessagingService
    {
        EmailResult SendMail(EmailMessage emailMessage);

        SmsResult SendSms(SmsMessage smsMessage);
    }

    public class MessagingService
        : IMessagingService
    {
        private readonly IEmailSender emailSender;
        private readonly ISmsSender smsSender;
        
        public MessagingService(
            IEmailSender emailSender,
            ISmsSender smsSender)
        {
            this.emailSender = emailSender;
            this.smsSender = smsSender;
        }

        public EmailResult SendMail(EmailMessage emailMessage)
        {
            EmailResult result = new EmailResult();
            result.Succeed = emailSender.Send(emailMessage);
            return result;
        }

        public SmsResult SendSms(SmsMessage smsMessage)
        {
            SmsResult result = new SmsResult();
            result.Succeed = true;
            smsSender.Send(smsMessage);
            return result;
        }
    }
}
