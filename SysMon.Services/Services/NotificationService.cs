﻿using SysMon.Models;
using FCM.Net;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SysMon.Services
{
    public interface INotificationService
    {
        NotificationResult Push(NotificationMessage notificationMessage);
    }

    public class NotificationService
        : INotificationService
    {
        public NotificationResult Push(NotificationMessage notificationMessage)
        {
            using (var sender = new Sender("AAAA_D4IKiw:APA91bEiGPJf9NnEajFnUg7B7DLn6tLbRdXnB0xv_nqGyY61MeWD_3kn2qyXCiUeTiEfMtNGBh0PlTLGZ4Yg1jRIGjteuEAZ9EU_ePcT0wbwWaBVUw7M4Wbv8spbJuluHtqJe5jWpjLm"))
            {
                var notification = new Message
                {
                    RegistrationIds = new List<string> { notificationMessage.DeviceId },
                    Notification = new Notification
                    {
                        Title = "Sysmon Notification",
                        Body = notificationMessage.Message,
                        Sound = notificationMessage.Sound
                    }
                };
                var result = sender.SendAsync(notification).Result;
                return new NotificationResult() { Succeed = true };
            }
        }

        private NotificationResult AndroidPush()
        {
            return new NotificationResult() { Succeed = true };
        }

        private NotificationResult IosPush()
        {
            return new NotificationResult() { Succeed = true };
        }
    }
}
