﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Identity;
using SysMon.Models;

namespace SysMon.Services
{
    public interface IRoleService
    {
        List<Role> GetAllRoles();
        IdentityResult Create(Role role);
        IdentityResult Delete(Role role);
        Role GetRoleByName(string roleName);
        Role GetRoleById(string id);
    }

    public class RoleService
        : IRoleService
    {
        private readonly RoleManager<Role> roleManager;

        public RoleService(RoleManager<Role> roleManager)
        {
            this.roleManager = roleManager;
        }

        public IdentityResult Create(Role role)
        {
            return this.roleManager.CreateAsync(role).Result;
        }

        public IdentityResult Delete(Role role)
        {
            return this.roleManager.DeleteAsync(role).Result;
        }

        public List<Role> GetAllRoles()
        {
            return this.roleManager.Roles.ToList();
        }

        public Role GetRoleByName(string roleName)
        {
            return roleManager.FindByNameAsync(roleName).Result;
        }

        public Role GetRoleById(string id)
        {
            return roleManager.FindByIdAsync(id.ToString()).Result;
        }
    }
}
