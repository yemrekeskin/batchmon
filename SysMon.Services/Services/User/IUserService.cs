﻿using Microsoft.AspNetCore.Identity;
using SysMon.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace SysMon.Services
{
    public interface IUserService
    {
        User Authenticate(string username, string password);

        IdentityResult Create(User user, string pass);
        IdentityResult Create(User user);
        IdentityResult Update(User user);
        IdentityResult UpdateDeviceId(User user);
        IdentityResult UpdateUserSound(User user);

        User GetUser();
        List<User> GetAllUsers();
        IEnumerable<User> GetAllUsers(Expression<Func<User, bool>> predicate);

        IdentityResult ResetPassword(long userId, string password);
        IdentityResult ChangePassword(User user, string password, string newPassword);
        User SignIn(string userName, string password);
        bool SignOut();

        Task<SignInResult> PasswordSignInAsync(string userName, string password, bool rememberMe);

        IdentityResult AddLogin(User user, UserLoginInfo userLoginInfo);
        ExternalLoginInfo GetExternalLoginInfo();
        User GetTwoFactorAuthenticationUser();
        IdentityResult AddPassword(User user, string newPassword);

        List<Role> GetUserRoles(User user);        
        IdentityResult AddUserToRole(User user, string roleName);
        IdentityResult RemoveUserFromRole(User user, string roleName);
        void RemoveUserFromRole(User user);

        IdentityResult ConfirmUserEmail(User user);
        bool IsEmailConfirmed(User user);

        IdentityResult ChangePhoneNumber(User user, string phoneNumber, string code);
        IdentityResult SetTwoFactorEnabled(User user);
        IdentityResult ConfirmEmailWithToken(User user, string confirmationToken);

        string GenerateChangePhoneNumberToken(User user, string phoneNumber);
        string GenerateEmailConfirmationToken(User user);
        string GeneratePasswordResetToken(User user);
        string GenerateTwoFactorToken(User user, string selectedProvider);

        IdentityResult ConfirmUserEmail(string userId, string confirmationToken);

        User GetUserById(string id);
        User GetUserByName(string name);

        IdentityResult ResetUserPassword(User user, string newPassword);
        string GetUserPasswordToken(long userId);
        User GetUserByEmail(string email);

        bool IsSignedIn(ClaimsPrincipal principal);
        bool HasPassword(User user);
        List<UserLoginInfo> GetLogins(User user);
        bool IsTwoFactorClientRemembered(User user);
        string GetPhoneNumber(User user);

        void RemovePassword(User user);
    }
}
