﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using SysMon.Models;

namespace SysMon.Services
{
    public class UserService
        : IUserService
    {
        protected readonly IHttpContextAccessor httpContextAccessor;
        protected readonly UserManager<User> userManager;
        protected readonly SignInManager<User> signInManager;
        protected readonly RoleManager<Role> roleManager;

        public UserService(
            IHttpContextAccessor httpContextAccessor,
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            RoleManager<Role> roleManager)
        {
            this.httpContextAccessor = httpContextAccessor;
            this.roleManager = roleManager;
            this.userManager = userManager;
            this.signInManager = signInManager;
        }
        
        public IdentityResult ChangePassword(User user, string password, string newPassword)
        {
            //var candidateUser = userManager.FindByNameAsync(user.UserName).Result;
            var passwordChangeResult = userManager.ChangePasswordAsync(user, password, newPassword).Result;

            return passwordChangeResult;
        }

        public IdentityResult AddPassword(User user, string newPassword)
        {
            return userManager.AddPasswordAsync(user, newPassword).Result;
        }

        public string GenerateTwoFactorToken(User user, string selectedProvider)
        {
           return userManager.GenerateTwoFactorTokenAsync(user, selectedProvider).Result;
        }

        public IdentityResult ChangePhoneNumber(User user, string phoneNumber, string code)
        {
           return userManager.ChangePhoneNumberAsync(user, phoneNumber, code).Result;
        }

        public IdentityResult SetTwoFactorEnabled(User user)
        {
            return userManager.SetTwoFactorEnabledAsync(user, true).Result;
        }

        public IdentityResult AddUserToRole(User user, string roleName)
        {
            var current = userManager.FindByIdAsync(user.Id.ToString()).Result;
            return userManager.AddToRoleAsync(current, roleName).Result;
        }

        public string GenerateChangePhoneNumberToken(User user, string phoneNumber)
        {
            return userManager.GenerateChangePhoneNumberTokenAsync(user, phoneNumber).Result;
        }

        public ExternalLoginInfo GetExternalLoginInfo()
        {
            return signInManager.GetExternalLoginInfoAsync().Result;
        }

        public IdentityResult AddLogin(User user, UserLoginInfo userLoginInfo)
        {
            return userManager.AddLoginAsync(user, userLoginInfo).Result;
        }

        public IdentityResult ConfirmEmailWithToken(User user, string confirmationToken)
        {
            return userManager.ConfirmEmailAsync(user, confirmationToken).Result;
        }
        public bool IsEmailConfirmed(User user)
        {
            return userManager.IsEmailConfirmedAsync(user).Result;
        }

        public IdentityResult ConfirmUserEmail(User user)
        {
            user.EmailConfirmed = true;
            return Update(user);
        }

        public IdentityResult ConfirmUserEmail(string userId, string confirmationToken)
        {
            var user = userManager.FindByIdAsync(userId).Result;
            if (user != null)
            {
                var identityResult = userManager.ConfirmEmailAsync(user, confirmationToken).Result;
                return identityResult;
            }
            return null;
        }

        public IdentityResult Create(User user,string pass)
        {
            var result = userManager.CreateAsync(user, pass).Result;
            return result;
        }

        public IdentityResult Create(User user)
        {
            var pass = Guid.NewGuid().ToString();
            var result = userManager.CreateAsync(user, pass).Result;
            return result;
        }

        public string GenerateEmailConfirmationToken(User user)
        {
            return userManager.GenerateEmailConfirmationTokenAsync(user).Result;
        }

        public string GeneratePasswordResetToken(User user)
        {
            return userManager.GeneratePasswordResetTokenAsync(user).Result;
        }

        public async Task<SignInResult> PasswordSignInAsync(string userName, string password, bool rememberMe)
        {
            var result = await signInManager.PasswordSignInAsync(userName, password, rememberMe, lockoutOnFailure: true);
            return result;
        }

        public List<User> GetAllUsers()
        {
            return userManager.Users.ToList();
        }

        public IEnumerable<User> GetAllUsers(Expression<Func<User, bool>> predicate)
        {
            return userManager.Users.Where(predicate);
        }

        public User GetUser()
        {
            //var userId = httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            return userManager.GetUserAsync(httpContextAccessor.HttpContext.User).Result;
        }

        public User GetUserByEmail(string email)
        {
            return userManager.FindByEmailAsync(email).Result;
        }

        public User GetUserById(string id)
        {
            return userManager.FindByIdAsync(id).Result;
        }

        public User GetUserByName(string name)
        {
            return userManager.FindByNameAsync(name).Result;
        }

        public string GetUserPasswordToken(long userId)
        {
            throw new NotImplementedException();
        }

        public List<Role> GetUserRoles(User user)
        {
            var list = new List<Role>();

            var systemRoles = roleManager.Roles;
            var userRoles = userManager.GetRolesAsync(user).Result;
            if (userRoles != null)
            {
                foreach (var userRole in userRoles)
                {
                    var role = systemRoles.FirstOrDefault(i => i.Name == userRole);
                    if (role != null)
                    {
                        list.Add(role);
                    }
                }
            }
            return list;
        }

        public IdentityResult RemoveUserFromRole(User user, string roleName)
        {
            var current = GetUserById(user.Id.ToString());
            return userManager.RemoveFromRoleAsync(current, roleName).Result;
        }

        public void RemoveUserFromRole(User user)
        {
            var current = GetUserById(user.Id.ToString());
            var userRoles = GetUserRoles(current);
            foreach (var role in userRoles)
            {
                userManager.RemoveFromRoleAsync(current, role.Name);
            }
        }

        public IdentityResult ResetPassword(long userId, string password)
        {
            var result = null as IdentityResult;
            var user = userManager.FindByIdAsync(userId.ToString()).Result;
            if (user != null)
            {
                var resetToken = userManager.GeneratePasswordResetTokenAsync(user).Result;
                result = userManager.ResetPasswordAsync(user, resetToken, password).Result;
                if (result.Succeeded)
                {
                    Update(user);                    
                }
            }
            return result;
        }

        public User GetTwoFactorAuthenticationUser()
        {
            return signInManager.GetTwoFactorAuthenticationUserAsync().Result;
        }

        public IdentityResult ResetUserPassword(User user, string newPassword)
        {
            var resetPasswordToken = userManager.GeneratePasswordResetTokenAsync(user).Result;
            return userManager.ResetPasswordAsync(user, resetPasswordToken, newPassword).Result;
        }

        public User SignIn(string email, string password)
        {
            var result = null as SignInResult;
            var user = userManager.FindByEmailAsync(email).Result;
            if (user != null)
            {
                result = signInManager.PasswordSignInAsync(user.UserName, password, true, false).Result;
                if (!result.Succeeded)                
                {
                    user = null;
                }
            }
            return user;
        }

        public IdentityResult UpdateDeviceId(User user)
        {
            var current = userManager.FindByIdAsync(user.Id.ToString()).Result;
            current.DeviceId = user.DeviceId;
            current.DeviceType = user.DeviceType;
            var result = userManager.UpdateAsync(current).Result;
            return result;
        }

        public IdentityResult UpdateUserSound(User user)
        {
            var current = userManager.FindByIdAsync(user.Id.ToString()).Result;
            current.Sound = user.Sound;
            var result = userManager.UpdateAsync(current).Result;
            return result;
        }

        public IdentityResult Update(User user)
        {
            //if (user.Password != "password" && !string.IsNullOrEmpty(user.Password))
            //{
            //    ResetPassword(user.Id, user.Password);
            //}

            //var current = userManager.FindByIdAsync(user.Id.ToString()).Result;
            //current.Email = user.Email;
            //current.EmailConfirmed = user.EmailConfirmed;
            //current.Name = user.Name;
            //current.PhoneNumber = user.PhoneNumber;
            //current.PhoneNumberConfirmed = user.PhoneNumberConfirmed;
            //current.GroupId = user.GroupId;
            //userManager.UpdateSecurityStampAsync(current).Wait();

            var result = userManager.UpdateAsync(user).Result;
            return result;
        }
        
        public bool SignOut()
        {
            return signInManager.SignOutAsync().IsCompleted;
        }

        public bool IsSignedIn(ClaimsPrincipal principal)
        {
            return this.signInManager.IsSignedIn(principal);
        }

        public bool HasPassword(User user)
        {
            return this.userManager.HasPasswordAsync(user).Result;
        }

        public List<UserLoginInfo> GetLogins(User user)
        {
            return this.userManager.GetLoginsAsync(user).Result.ToList();
        }

        public bool IsTwoFactorClientRemembered(User user)
        {
            return signInManager.IsTwoFactorClientRememberedAsync(user).Result;
        }

        public string GetPhoneNumber(User user)
        {
            return userManager.GetPhoneNumberAsync(user).Result;
        }

        public User Authenticate(string username, string password)
        {
            User user = null;
            if (username.IsEmailValid())
            {
                user = userManager.FindByEmailAsync(username).Result;
            }
            else
            {
                user = userManager.FindByNameAsync(username).Result;
            }
            //var user = userManager.Users.SingleOrDefault(d => d.UserName == username && d.Password == password);

            // return null if user not found
            if (user == null) return null;

            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();

            string secret = SystemConstants.Secret;
            //var key = Encoding.ASCII.GetBytes(secret);

            var claims = new[]
            {
                  new Claim(ClaimTypes.Name, user.Id.ToString()),
                  new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
                  new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                  new Claim("Id", user.Id.ToString()),
                  new Claim("UserName", user.UserName),
                  new Claim("Email", user.Email),
                  new Claim("SecurityStamp", user.SecurityStamp ?? string.Empty),
                  new Claim("Name", user.Name ?? string.Empty),
                  new Claim("Surname", user.Surname ?? string.Empty),
                  new Claim("EmailConfirmed", user.EmailConfirmed.ToString()),
                  new Claim("PhoneNumberConfirmed", user.PhoneNumberConfirmed.ToString())
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secret));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
              "msysmon.com",
              "msysmon.com",
              claims,
              expires: DateTime.Now.AddMinutes(30),
              signingCredentials: creds);

            //var tokenDescriptor = new SecurityTokenDescriptor
            //{
            //    Subject = new ClaimsIdentity(new Claim[]
            //    {
            //        new Claim(ClaimTypes.Name, user.Id.ToString()),
            //        new Claim(JwtRegisteredClaimNames.Sub, user.UserName),
            //        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            //        new Claim("Id", user.Id.ToString()),
            //        new Claim("UserName", user.UserName),
            //        new Claim("Email", user.Email),
            //        new Claim("SecurityStamp", user.SecurityStamp ?? string.Empty),
            //        new Claim("Name", user.Name ?? string.Empty),
            //        new Claim("Surname", user.Surname ?? string.Empty),
            //        new Claim("EmailConfirmed", user.EmailConfirmed.ToString()),
            //        new Claim("PhoneNumberConfirmed", user.PhoneNumberConfirmed.ToString())                       
            //    }),
            //    Expires = DateTime.UtcNow.AddDays(7),
            //    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            //};

            //var token = tokenHandler.CreateToken(tokenDescriptor);
            string generatedToken = tokenHandler.WriteToken(token);
            user.Token = generatedToken;

            AddUserToken(user, generatedToken);
            
            return user;
        }

        private void AddUserToken(User user, string token)
        {
            IdentityResult result = userManager.UpdateAsync(user).Result;
        }

        public void RemovePassword(User user)
        {
            IdentityResult result = userManager.RemovePasswordAsync(user).Result;
        }
    }
}
