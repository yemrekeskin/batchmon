﻿using SysMon.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SysMon.Services
{
    public interface IWatcherService
       : IService<Watcher>
    {

    }

    public class WatcherService
        : BaseService<Watcher>, IWatcherService
    {
        public WatcherService(IWatcherRepository repo)
            : base(repo)
        {

        }
    }
}
