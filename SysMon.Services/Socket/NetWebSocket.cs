﻿using System;
using System.Collections.Generic;
using System.Net.WebSockets;
using System.Text;

namespace SysMon.Services
{
    /// <summary>
    /// FXTrading web socket class definition.
    /// </summary>
    public class NetWebSocket
    {
        /// <summary>
        /// Gets or sets Id.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Socket specific name.
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// Gets or sets WebSocket.
        /// </summary>
        public WebSocket WebSocket { get; set; }

        /// <summary>
        /// Gets or sets SocketType.
        /// </summary>
        public SocketType SocketType { get; set; }

        /// <summary>
        /// Gets or sets IpAddress.
        /// </summary>
        public string IpAddress { get; set; }

        /// <summary>
        /// Gets or sets UserName.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets Culture
        /// </summary>
        public string Culture { get; set; }

        /// <summary>
        /// Equals object
        /// </summary>
        /// <param name="obj">Clas</param>
        /// <returns>Boolean.</returns>
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            var socket = obj as NetWebSocket;

            if (socket.Id == this.Id)
            {
                return true;
            }
            else {
                return false;
            }
        }

        /// <summary>
        /// Hash Code Getter.
        /// </summary>
        /// <returns>Hash Code.</returns>
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}
