﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SysMon.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading.Tasks;

namespace SysMon.Services
{
    public class NotificationHandler 
        : WebSocketHandler
    {
        private readonly IJobService jobService;

        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationHandler"/> class.
        /// </summary>
        /// <param name="webSocketConnectionManager">Web Socket Manager.</param>
        public NotificationHandler(
            WebSocketConnectionManager webSocketConnectionManager, 
            ILoggerFactory loggerFactory,
            IJobService jobService)
            : base(webSocketConnectionManager, loggerFactory)
        {
            this.jobService = jobService;
        }

        /// <summary>
        /// When data received.
        /// </summary>
        /// <param name="socket">Web socket instance.</param>
        /// <param name="result">Result of data receive.</param>
        /// <param name="buffer">Buffer of data.</param>
        /// <returns>Void.</returns>
        public override async Task ReceiveAsync(NetWebSocket socket, WebSocketReceiveResult result, byte[] buffer)
        {
            var socketId = WebSocketConnectionManager.GetId(socket);
            long clientId = 0;// long.Parse(socket.ClientId); // userid

            var messageJson = Encoding.UTF8.GetString(buffer, 0, result.Count);
            //IEnumerable<Job> data = jobService.List().Where(d => d.UserId == clientId);
            var data = 0;

            await SendDataToAllAsync(data);
        }
    }
}
