﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SysMon.Services
{
    public enum SocketType
    {
        /// <summary>
        /// Global notification web socket.
        /// </summary>
        Notification = 0,

        /// <summary>
        /// Currency web socket.
        /// </summary>
        Currency = 1
    }
}
