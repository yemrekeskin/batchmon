﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SysMon.Services
{
    /// <summary>
    /// Web socket connection manager.
    /// </summary>
    //[SingletonDependency(ServiceType = typeof(WebSocketConnectionManager))]
    public class WebSocketConnectionManager
    {
        private ConcurrentDictionary<string, NetWebSocket> sockets = new ConcurrentDictionary<string, NetWebSocket>();

        private Guid guid { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="WebSocketConnectionManager"/> class.
        /// </summary>
        public WebSocketConnectionManager() {
            guid = Guid.NewGuid();
        }

        /// <summary>
        /// Find sockect with id.
        /// </summary>
        /// <param name="id">Socket unique identity.</param>
        /// <returns>Web Socket Instance.</returns>
        public NetWebSocket GetSocketById(string id)
        {
            return sockets.FirstOrDefault(p => p.Key == id).Value;
        }

        /// <summary>
        /// All socket items.
        /// </summary>
        /// <returns>Web socket list.</returns>
        public List<NetWebSocket> GetAll() {
            return sockets.Select(i => i.Value).ToList();
        }

        /// <summary>
        /// All socket type items.
        /// </summary>
        /// <param name="type">Socket type.</param>
        /// <returns>Web socket list.</returns>
        public List<NetWebSocket> GetSocketByType(SocketType type)
        {
            return sockets.Where(i => i.Value.SocketType == type).Select(i => i.Value).ToList();
        }

        /// <summary>
        /// Socket Id.
        /// </summary>
        /// <param name="socket">Socket Instance.</param>
        /// <returns>Socket unique identity.</returns>
        public string GetId(NetWebSocket socket)
        {
            return sockets.FirstOrDefault(p => p.Value == socket).Key;
        }

        /// <summary>
        /// Socket exists or not.
        /// </summary>
        /// <param name="userName">Current user name.</param>
        /// <param name="ipAddress">Current ip addess.</param>
        /// <param name="cliendId">Client id.</param>
        /// <returns>Boolean.</returns>
        public bool IsExists(string userName, string ipAddress, string cliendId) {
            var list = GetAll();
            return list.Any(i => i.UserName == userName && i.IpAddress == ipAddress && i.ClientId == cliendId);
        }

        /// <summary>
        /// Add new socket to list.
        /// </summary>
        /// <param name="socket">Socket instance.</param>
        /// <returns>Void</returns>
        public async Task<NetWebSocket> AddSocket(NetWebSocket socket)
        {
            await Task.Run(() => sockets.TryAdd(socket.Id, socket));
            return socket;
        }

        /// <summary>
        /// It removes sockect from list.
        /// </summary>
        /// <param name="id">Socket unique identity.</param>
        /// <returns>Web Socket Instance.</returns>
        public async Task RemoveSocket(string id)
        {
            NetWebSocket socket;
            sockets.TryRemove(id, out socket);

            await socket.WebSocket.CloseAsync(
                closeStatus: WebSocketCloseStatus.NormalClosure,
                statusDescription: "Closed by the WebSocketManager",
                cancellationToken: CancellationToken.None);

            await Task.CompletedTask;
        }
    }
}
