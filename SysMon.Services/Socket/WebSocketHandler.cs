﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace SysMon.Services
{
    /// <summary>
    /// Web Socket Handler.
    /// </summary>
    public abstract class WebSocketHandler
    {
        /// <summary>
        /// Web socket connection manager.
        /// </summary>
        protected WebSocketConnectionManager WebSocketConnectionManager { get; set; }

        private ILogger logger { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="WebSocketHandler"/> class.
        /// </summary>
        /// <param name="webSocketConnectionManager">Web socket connection manager.</param>
        /// <param name="loggerFactory">Logger factory.</param>
        public WebSocketHandler(WebSocketConnectionManager webSocketConnectionManager, ILoggerFactory loggerFactory)
        {
            WebSocketConnectionManager = webSocketConnectionManager;
            logger = loggerFactory.CreateLogger("WebSocketHandler");
        }

        /// <summary>
        /// Socket connected
        /// </summary>
        /// <param name="context">WebSocket.</param>
        /// <returns>Void.</returns>
        public virtual async Task<NetWebSocket> OnConnected(HttpContext context)
        {
            var socket = await context.WebSockets.AcceptWebSocketAsync();
            var parsed = HttpUtility.ParseQueryString(context.Request.QueryString.Value);
            //var userName = parsed["userName"];
            parsed["type"] = "Notification";
            //parsed["clientid"] = ""; // userid //"127.0.0.1";
            //var userName = "YunusEmre";
            var netSocket = new NetWebSocket()
            {
                Id = Guid.NewGuid().ToString(),
                SocketType = (SocketType)Enum.Parse(typeof(SocketType), parsed["type"]),
                ClientId = parsed["clientid"],
                WebSocket = socket,
                //UserName = userName,
                //Culture = user.Culture,
                IpAddress = context.Connection.RemoteIpAddress.ToString()
            };
            return await WebSocketConnectionManager.AddSocket(netSocket);
        }

        /// <summary>
        /// Current request is exist or not.
        /// </summary>
        /// <param name="context">Current http context.</param>
        /// <returns>If it is exists return true.</returns>
        public virtual bool IsExists(HttpContext context) {
            return false;
            //TODO:check client identity
            //var parsed = HttpUtility.ParseQueryString(context.Request.QueryString.Value);
            //var user = context.User.GetUser();
            //return WebSocketConnectionManager.IsExists(
            //    user.UserName,
            //    context.Connection.RemoteIpAddress.ToString(),
            //    parsed["clientid"]);
        }

        /// <summary>
        /// Socket dis connected.
        /// </summary>
        /// <param name="socket">Socket Instance.</param>
        /// <returns>Void.</returns>
        public virtual async Task OnDisconnected(NetWebSocket socket)
        {
            await WebSocketConnectionManager.RemoveSocket(WebSocketConnectionManager.GetId(socket));
        }

        /// <summary>
        /// Send Message to connected socket
        /// </summary>
        /// <param name="socket">Socket Instance.</param>
        /// <param name="data">Message</param>
        /// <returns>Void.</returns>
        public async Task SendDataAsync(NetWebSocket socket, object data)
        {
            try
            {
                //var defaultCulture = SupportedCultures.Cultures.First();
                //var culture = System.Globalization.CultureInfo.GetCultureInfo(defaultCulture.Value.ToString());
                //var datajson = JsonConvert.SerializeObject(data, new JSONSettings(culture).Settings);
                var datajson = JsonConvert.SerializeObject(data);
                if (socket.WebSocket.State != WebSocketState.Open)
                {
                    return;
                }

                await socket.WebSocket.SendAsync(
                    buffer: new ArraySegment<byte>(
                        array: Encoding.ASCII.GetBytes(datajson),
                        offset: 0,
                        count: datajson.Length),
                        messageType: WebSocketMessageType.Text,
                        endOfMessage: true,
                        cancellationToken: CancellationToken.None);
            }
            catch (Exception ex) {
                logger.LogError(ex.Message, ex);
            }
        }

        /// <summary>
        /// Send Message to connected socket
        /// </summary>
        /// <param name="socketId">Socket unique identity.</param>
        /// <param name="data">Message which will be disturb.</param>
        /// <returns>Void.</returns>
        public async Task SendDataAsync(string socketId, object data)
        {
            var socket = WebSocketConnectionManager.GetSocketById(socketId);
            await SendDataAsync(socket, data);
        }

        /// <summary>
        /// Send Message to all connected sockets.
        /// </summary>
        /// <param name="data">Message which will be disturb.</param>
        /// <returns>Void.</returns>
        public async Task SendDataToAllAsync(object data)
        {
            var sockets = WebSocketConnectionManager.GetAll();
            var tasks = sockets.Select(async socket => {
                if (socket.WebSocket.State == WebSocketState.Open)
                {
                    await SendDataAsync(socket, data);
                }
            });
            await Task.WhenAll(tasks);
        }

        /// <summary>
        /// Send Message to all connected sockets.
        /// </summary>
        /// <param name="data">Message which will be disturb.</param>
        /// /// <param name="type">Socket type.</param>
        /// <returns>Void.</returns>
        public async Task SendDataToTypeAsync(object data, SocketType type)
        {
            var sockets = WebSocketConnectionManager.GetAll().Where(i => i.SocketType == type).ToList();
            var tasks = sockets.Select(async socket => {
                if (socket.WebSocket.State == WebSocketState.Open)
                {
                    await SendDataAsync(socket, data);
                }
            });
            await Task.WhenAll(tasks);
        }

        /// <summary>
        /// Receive data method.
        /// </summary>
        /// <param name="socket">Socket Instance.</param>
        /// <param name="result">Receive data result.</param>
        /// <param name="buffer">Buffer of data.</param>
        /// <returns>Void.</returns>
        public abstract Task ReceiveAsync(NetWebSocket socket, WebSocketReceiveResult result, byte[] buffer);
    }
}
