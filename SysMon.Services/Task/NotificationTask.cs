﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using SysMon.Models;
using SysMon.Models.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SysMon.Services
{
    public class NotificationTask
        : IScheduledTask
    {
        public string Schedule => "*/2 * * * *";

        private readonly ApplicationDbContext _context;

        private readonly INotificationService notificationService;
        private readonly UserManager<User> userManager;
        private readonly IJobService jobService;
        private readonly IJobConfigService jobConfigService;
        private readonly IJobScheduleService jobScheduleService;
        private readonly IJobNotificationService jobNotificationService;

        public NotificationTask(
            ApplicationDbContext _context,
            IServiceProvider serviceProvider,
            UserManager<User> userManager,
            INotificationService notificationService,
            IJobService jobService,
            IJobConfigService jobConfigService,
            IJobScheduleService jobScheduleService,
            IJobNotificationService jobNotificationService)
        {
            this._context = _context;

            this.notificationService = notificationService;
            this.userManager = userManager;
            this.jobService = jobService;
            this.jobConfigService = jobConfigService;
            this.jobNotificationService = jobNotificationService;
            this.jobScheduleService = jobScheduleService;
        }

        public void Execute(CancellationToken cancellationToken)
        {

            ///// 

            // Hata alan tüm jobların listesini çek
            var failedJobs = jobService.List().Where(d => d.JobState == JobState.Failed);
            //var failedJobs = jobService.GetFailedJobs();
            foreach (var failedJob in failedJobs)
            {
                var failedJobId = failedJob.Id;
                var failedJobRunId = failedJob.JobRunId;
                var JobName = failedJob.JobName;

                // Hata alan bu job ın konfigure olduğu kullanıcıları bul
                var failedJobConfigs = jobConfigService.List().Where(d => d.JobId == failedJobId);
                foreach (var failedJobConfig in failedJobConfigs)
                {
                    var failedJobConfigUserId = failedJobConfig.UserId;
                    // Kullanıcı bildirim almak istiyor mu ?
                    bool notifyOnFailure = failedJobConfig.NotifyOnFailure;

                    // Daha önce notification gitmiş mi ?
                    var jobNotification = jobNotificationService.List()
                        .Where(d => d.JobId == failedJobId && d.JobRunId == failedJobRunId && d.UserId == failedJobConfigUserId);
                    bool isNotify = jobNotification.Count() == 0 ? true : false;

                    if (isNotify && notifyOnFailure)
                    {
                        // Kullanıcının bildirim tarihi mi ?
                        bool isScheduledForThisUser = this.IsScheduledForThisJob(failedJobConfigUserId);
                        if (isScheduledForThisUser == true)
                        {
                            var userId = failedJobConfigUserId;
                            //var user = userManager.FindByIdAsync(userId.ToString()).Result;
                            var user = _context.Users.AsNoTracking().FirstOrDefault(d => d.Id == userId);
                            if (user != null)
                            {
                                string userSound = user.Sound == null ? Sounds.Siren : user.Sound;
                                var soundPrefered = String.IsNullOrEmpty(userSound) ? Sounds.Siren : userSound.Trim();
                                //soundPrefered = Sounds.Ticktock;

                                // bildirim gönder
                                string message = JobName;
                                var notify = new NotificationMessage()
                                {
                                    DeviceId = user.DeviceId,
                                    DeviceType = user.DeviceType,
                                    Message = message,
                                    Sound = soundPrefered
                                };
                                notificationService.Push(notify);

                                // Bildirim gönderildi logu insert
                                var notification = new JobNotification();
                                notification.JobId = failedJobId;
                                notification.JobRunId = failedJob.JobRunId;
                                notification.LastSentDate = DateTime.Now;
                                notification.Message = message;
                                notification.NotifyState = NotifyState.Sent;
                                notification.Title = "Sysmon Notification";
                                notification.UserId = userId;
                                notification.Sound = soundPrefered;
                                jobNotificationService.Add(notification);
                            }
                        }
                    }
                }
            }
        }

        public bool IsScheduledForThisJob(string userId)
        {
            var scheduledList = jobScheduleService.List().Where(d => d.UserId == userId);
            if(scheduledList != null)
            {
                var infinite = new DateTime(9999, 1, 1);
                var infiniteScheduled = scheduledList.Where(d => d.DateTime == infinite);
                if(infiniteScheduled.Count() > 0)
                {
                    return true;
                }

                var today = DateTime.Now;
                var sheduledToday = new DateTime(today.Year, today.Month, today.Day);
                var scheduled = scheduledList.Where(d => d.DateTime == sheduledToday);
                if(scheduled.Count() > 0)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
