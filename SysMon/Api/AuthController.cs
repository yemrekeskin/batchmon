﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SysMon.ManageViewModels;
using SysMon.Models;
using SysMon.Services;
using SysMon.ServiceViewModels;
using SysMon.ViewModels;

namespace SysMon.Api
{
    [Route("api/v1/auth")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class AuthController 
        : ControllerBase
    {
        private readonly ILogger<AuthController> logger;
        private readonly IMapper mapper;
        private readonly IUserService userService;
        private readonly IRoleService roleService;
        private readonly IGroupService groupService;

        public AuthController(
            ILogger<AuthController> logger,
            IMapper mapper,
            IUserService userService,
            IRoleService roleService,
            IGroupService groupService)
        {
            this.logger = logger;
            this.mapper = mapper;
            this.userService = userService;
            this.roleService = roleService;
            this.groupService = groupService;
        }

        [AllowAnonymous]
        [HttpPost("token")]
        public IActionResult Authenticate([FromBody] AuthViewModel loginViewModel)
        {
            var user = userService.Authenticate(loginViewModel.UserName, loginViewModel.Password);

            if(user  == null)
            {
                return Unauthorized();
            }

            if(!String.IsNullOrEmpty(loginViewModel.DeviceId)) {
                user.DeviceId = loginViewModel.DeviceId;
                user.DeviceType = loginViewModel.DeviceType;
                userService.UpdateDeviceId(user);
            }

            string roleName = String.Empty;
            var userRoles = userService.GetUserRoles(user);
            if(userRoles.Count > 0)
            {
                roleName = userRoles[0].Name;
            }

            string groupName = user.GroupId != 0 ? groupService.Get(user.GroupId).GroupName : String.Empty;

            var userViewModel = new ProfileViewModel();
            userViewModel.Name = user.Name;
            userViewModel.UserName = user.UserName;
            userViewModel.Token = user.Token;
            userViewModel.DeviceId = user.DeviceId;
            userViewModel.Email = user.Email;
            userViewModel.RoleName = roleName;
            userViewModel.GroupName = groupName;

            return Ok(userViewModel);
        }

        [AllowAnonymous]
        [HttpPost("AddDeviceIdToUser")]
        public IActionResult AddDeviceIdToUser([FromBody] AuthViewModel loginViewModel)
        {
            var user = userService.GetUserByName(loginViewModel.UserName);
            if (user == null)
            {
                return Unauthorized();
            }
            if (!String.IsNullOrEmpty(loginViewModel.DeviceId))
            {
                user.DeviceId = loginViewModel.DeviceId;
                user.DeviceType = loginViewModel.DeviceType;
                userService.UpdateDeviceId(user);
            }
            return Ok();
        }

        [HttpPost("AddUserProperties")]
        public IActionResult AddUserProperties([FromBody] UserProperties userProperties)
        {
            try
            {
                var user = GetCurrentUser();
                if (user != null)
                {
                    if(userProperties != null)
                    {
                        if (!String.IsNullOrEmpty(userProperties.Sound))
                        {
                            user.Sound = userProperties.Sound;
                            var result = userService.UpdateUserSound(user);
                            if (result.Succeeded)
                            {
                                return Ok("Successfully User Sound Changed");
                            }
                            AddErrors(result);
                        }
                        return BadRequest(ModelState);
                    }                 
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
            return BadRequest("Internal Error");
        }

        [HttpPost("changepassword")]
        public IActionResult ChangePassword([FromBody] ChangePasswordViewModel changePasswordViewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var user = GetCurrentUser();
                if (user != null)
                {
                    if (userService.HasPassword(user))
                    {
                        userService.RemovePassword(user);
                        var result = userService.AddPassword(user, changePasswordViewModel.ConfirmPassword);
                        if (result.Succeeded)
                        {
                            return Ok("Successfully Change Password");
                        }
                        AddErrors(result);
                    }
                    return BadRequest(ModelState);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
            return BadRequest("Internal Error");
        }

        private User GetCurrentUser()
        {
            return userService.GetUser();
        }

        [HttpGet("{id}", Name = "profile")]
        public IActionResult GetUserProfile([FromBody] UserProfileSearchViewModel userProfileSearch)
        {
            UserProfileViewModel result = new UserProfileViewModel();
            User user = GetCurrentUser();

            if(user != null)
            {
                result.Name = user.Name;
                result.Surname = user.Surname;
            }
            return Ok(result);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }
    }
}