﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SysMon.Models;
using SysMon.Services;
using SysMon.ServiceViewModels;
using SysMon.ViewModels;

namespace SysMon.Api
{
    [Route("api/v1/groups")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class GroupController 
        : ControllerBase
    {
        private readonly ILogger<GroupController> logger;
        private readonly IMapper mapper;
        private readonly IUserService userService;
        private readonly IGroupService groupService;

        public GroupController(
            ILogger<GroupController> logger,
            IMapper mapper,
            IUserService userService,
            IGroupService groupService)
        {
            this.logger = logger;
            this.mapper = mapper;
            this.userService = userService;
            this.groupService = groupService;
        }


        [HttpGet]
        public IActionResult Get()
        {
            var list = groupService.List();
            var result = mapper.Map<IEnumerable<GroupViewModel>>(list);
            return Ok(result);
        }

        [HttpGet("{id}", Name = "GetGroup")]
        public IActionResult Get(int id)
        {
            var group = groupService.Get(id);
            if (group == null)
            {
                return NotFound();
            }
            var result = mapper.Map<GroupViewModel>(group);
            return Ok(result);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var group = groupService.Get(id);
            if (group == null)
            {
                return NotFound();
            }

            groupService.Remove(group);
            return NoContent();
        }

        [HttpPost()]
        public IActionResult Post([FromBody] GroupViewModel groupViewModel)
        {
            if (groupViewModel == null)
            {
                return BadRequest();
            }

            var group = mapper.Map<Group>(groupViewModel);
            groupService.Add(group);

            if (group.Id.Equals(0))
            {
                return StatusCode(500, "Error");
            }

            var result = mapper.Map<GroupViewModel>(group);
            return CreatedAtRoute("GetGroup", new { result.Id }, result);
        }

        [HttpPut("{id}")]
        public IActionResult Put(long id, [FromBody] GroupViewModel groupViewModel)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            if (groupViewModel == null)
            {
                return BadRequest();
            }

            var group = groupService.Get(id);
            if (group == null)
            {
                return NotFound();
            }

            try
            {
                group.Explanation = groupViewModel.Explanation;
                group.GroupName = groupViewModel.GroupName;

                groupService.Update(group);

                //return Ok();
                return NoContent();
            }
            catch (Exception)
            {
                return NotFound();
            }
        }
    }
}