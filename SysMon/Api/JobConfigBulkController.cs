﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding.Binders;
using Microsoft.Extensions.Logging;
using SysMon.Models;
using SysMon.Services;
using SysMon.ServiceViewModels;
using SysMon.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SysMon.Api
{
    [Route("api/v1/bulk/jobconfigs")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class JobConfigBulkController
           : ControllerBase
    {
        private readonly ILogger<JobConfigController> logger;
        private readonly IMapper mapper;
        private readonly IUserService userService;
        private readonly IJobConfigService jobConfigService;

        public JobConfigBulkController(
            ILogger<JobConfigController> logger,
            IMapper mapper,
            IUserService userService,
            IJobConfigService jobConfigService)
        {
            this.logger = logger;
            this.mapper = mapper;
            this.userService = userService;
            this.jobConfigService = jobConfigService;
        }

        [HttpPost]
        public IActionResult Post([FromBody] IEnumerable<JobConfigViewModel> questionViewModels)
        {
            if (questionViewModels == null)
            {
                return BadRequest();
            }

            var questions = mapper.Map<IEnumerable<JobConfig>>(questionViewModels);

            foreach (var question in questions)
            {
                jobConfigService.Add(question);
            }

            var questionsToReturn = mapper.Map<IEnumerable<JobConfigViewModel>>(questions);
            var idsAsString = string.Join(",",
                questionsToReturn.Select(a => a.Id));

            return CreatedAtRoute("GetJobConfigCollection",
                new { ids = idsAsString },
                questionsToReturn);
        }

        [HttpGet("({ids})", Name = "GetJobConfigCollection")]
        public IActionResult GetCollection(
            [ModelBinder(BinderType = typeof(ArrayModelBinder))] IEnumerable<long> ids)
        {
            if (ids == null)
            {
                return BadRequest();
            }
            var list = jobConfigService.List(ids);

            if (ids.Count() != list.Count())
            {
                return NotFound();
            }

            var questionsToReturn = Mapper.Map<IEnumerable<JobConfigViewModel>>(list);
            return Ok(questionsToReturn);
        }
    }
}
