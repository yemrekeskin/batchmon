﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SysMon.Models;
using SysMon.Services;
using SysMon.ServiceViewModels;
using SysMon.ViewModels;

namespace SysMon.Api
{
    [Route("api/v1/jobconfigs")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class JobConfigController 
        : ControllerBase
    {
        private readonly ILogger<JobConfigController> logger;
        private readonly IMapper mapper;
        private readonly IUserService userService;
        private readonly IJobConfigService jobConfigService;

        public JobConfigController(
            ILogger<JobConfigController> logger,
            IMapper mapper,
            IUserService userService,
            IJobConfigService jobConfigService)
        {
            this.logger = logger;
            this.mapper = mapper;
            this.userService = userService;
            this.jobConfigService = jobConfigService;
        }


        [HttpGet]
        public IActionResult Get()
        {
            var currentUser = userService.GetUser();
            if (currentUser != null)
            {
                var userId = currentUser.Id;

                var list = jobConfigService.List(d => d.UserId == userId);
                var result = mapper.Map<IEnumerable<JobConfigViewModel>>(list);
                return Ok(result);
            }
            return NoContent();
        }

        [HttpGet("{id}", Name = "GetJobConfig")]
        public IActionResult Get(long id)
        {
            var jobConfig = jobConfigService.Get(id);
            if (jobConfig == null)
            {
                return NotFound();
            }
            var result = mapper.Map<JobConfigViewModel>(jobConfig);
            return Ok(result);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            var jobConfig = jobConfigService.Get(id);
            if (jobConfig == null)
            {
                return NotFound();
            }

            jobConfigService.Remove(jobConfig);
            return NoContent();
        }

        [HttpPost()]
        public IActionResult Post([FromBody] JobConfigViewModel jobViewModel)
        {
            if (jobViewModel == null)
            {
                return BadRequest();
            }

            var jobConfig = mapper.Map<JobConfig>(jobViewModel);
            jobConfigService.Add(jobConfig);

            if (jobConfig.Id.Equals(0))
            {
                return StatusCode(500, "Error");
            }

            var result = mapper.Map<JobConfigViewModel>(jobConfig);
            return CreatedAtRoute("GetJobConfig", new { result.Id }, result);
        }

        [HttpPut("{id}")]
        public IActionResult Put(long id, [FromBody] JobConfigViewModel jobConfigViewModel)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            if (jobConfigViewModel == null)
            {
                return BadRequest();
            }

            var jobConfig = jobConfigService.Get(id);
            if (jobConfig == null)
            {
                return NotFound();
            }

            try
            {
                jobConfig.Hot = jobConfigViewModel.Hot;
                jobConfig.NotifyOnDelay = jobConfigViewModel.NotifyOnDelay;
                jobConfig.NotifyOnFailure = jobConfigViewModel.NotifyOnFailure;
                jobConfig.ThresholdForDelay = jobConfigViewModel.ThresholdForDelay;

                jobConfigService.Update(jobConfig);

                //return Ok();
                return NoContent();
            }
            catch (Exception)
            {
                return NotFound();
            }
        }
    }
}