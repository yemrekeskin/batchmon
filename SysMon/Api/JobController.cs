﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SysMon.Models;
using SysMon.Services;
using SysMon.ServiceViewModels;
using SysMon.ViewModels;

namespace SysMon.Api
{
    [Route("api/v1/jobs")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class JobController 
        : ControllerBase
    {
        private readonly ILogger<JobController> logger;
        private readonly IMapper mapper;
        private readonly IUserService userService;
        private readonly IJobService jobService;
        private readonly IJobConfigService jobConfigService;
        
        public JobController(
            ILogger<JobController> logger,
            IMapper mapper,
            IUserService userService,
            IJobService jobService,
            IJobConfigService jobConfigService)
        {
            this.logger = logger;
            this.mapper = mapper;
            this.userService = userService;
            this.jobService = jobService;
            this.jobConfigService = jobConfigService;
        }


        [HttpGet]
        public IActionResult Get()
        {
            var currentUser = userService.GetUser();
            if (currentUser != null)
            {
                var userId = currentUser.Id;
                var list = jobService.List();
                var result = mapper.Map<IEnumerable<JobViewModel>>(list);
                return Ok(result);
            }
            return NoContent();            
        }

        [HttpGet("{id}", Name = "GetJob")]
        public IActionResult Get(long id)
        {
            var job = jobService.Get(id);
            if (job == null)
            {
                return NotFound();
            }
            var result = mapper.Map<JobViewModel>(job);
            return Ok(result);
        }



        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            var job = jobService.Get(id);
            if (job == null)
            {
                return NotFound();
            }

            jobService.Remove(job);
            return NoContent();
        }

        [HttpPost()]
        public IActionResult Post([FromBody] JobViewModel jobViewModel)
        {
            if (jobViewModel == null)
            {
                return BadRequest();
            }

            var job = mapper.Map<Job>(jobViewModel);
            jobService.Add(job);

            if (job.Id.Equals(0))
            {
                return StatusCode(500, "Error");
            }

            var result = mapper.Map<JobViewModel>(job);
            return CreatedAtRoute("GetJob", new { result.Id }, result);
        }

        [HttpPut("{id}")]
        public IActionResult Put(long id, [FromBody] JobViewModel jobViewModel)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            if (jobViewModel == null)
            {
                return BadRequest();
            }

            var job = jobService.Get(id);
            if (job == null)
            {
                return NotFound();
            }

            try
            {
                job.JobName = jobViewModel.JobName;
                job.JobState = jobViewModel.JobState;
                job.StartTime = jobViewModel.StartTime;
                job.EndTime = jobViewModel.EndTime;
                job.Delay = jobViewModel.Delay;

                jobService.Update(job);

                //return Ok();
                return NoContent();
            }
            catch (Exception)
            {
                return NotFound();
            }
        }
    }
}