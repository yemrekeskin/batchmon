﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SysMon.Models;
using SysMon.Services;
using SysMon.ServiceViewModels;
using SysMon.ViewModels;

namespace SysMon.Api
{
    [Route("api/v1/jobnotifications")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class JobNotificationController 
        : ControllerBase
    {
        private readonly ILogger<JobNotificationController> logger;
        private readonly IMapper mapper;
        private readonly IUserService userService;
        private readonly IJobNotificationService jobNotificationService;
        
        public JobNotificationController(
            ILogger<JobNotificationController> logger,
            IMapper mapper,
            IUserService userService,
            IJobNotificationService jobNotificationService)
        {
            this.logger = logger;
            this.mapper = mapper;
            this.userService = userService;
            this.jobNotificationService = jobNotificationService;
        }


        [HttpGet]
        public IActionResult Get()
        {
            var currentUser = userService.GetUser();
            if (currentUser != null)
            {
                var userId = currentUser.Id;
                var list = jobNotificationService.List(d=>d.UserId == userId);
                var result = mapper.Map<IEnumerable<JobNotificationViewModel>>(list);
                return Ok(result);
            }
            return NoContent();
        }

        [HttpGet("{id}", Name = "GetJobNotification")]
        public IActionResult Get(long id)
        {
            var jobNotification = jobNotificationService.Get(id);
            if (jobNotification == null)
            {
                return NotFound();
            }
            var result = mapper.Map<JobNotificationViewModel>(jobNotification);
            return Ok(result);
        }
        
        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            var jobNotification = jobNotificationService.Get(id);
            if (jobNotification == null)
            {
                return NotFound();
            }

            jobNotificationService.Remove(jobNotification);
            return NoContent();
        }

        [HttpPost()]
        public IActionResult Post([FromBody] JobNotificationViewModel jobNotificationViewModel)
        {
            if (jobNotificationViewModel == null)
            {
                return BadRequest();
            }

            var jobNotification = mapper.Map<JobNotification>(jobNotificationViewModel);
            jobNotificationService.Add(jobNotification);

            if (jobNotification.Id.Equals(0))
            {
                return StatusCode(500, "Error");
            }

            var result = mapper.Map<JobNotificationViewModel>(jobNotification);
            return CreatedAtRoute("GetJobNotification", new { result.Id }, result);
        }

        [HttpPut("{id}")]
        public IActionResult Put(long id, [FromBody] JobNotificationViewModel jobNotificationViewModel)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            if (jobNotificationViewModel == null)
            {
                return BadRequest();
            }

            var jobNotification = jobNotificationService.Get(id);
            if (jobNotification == null)
            {
                return NotFound();
            }

            try
            {
                jobNotification.Title = jobNotificationViewModel.Title;
                jobNotification.Message = jobNotificationViewModel.Message;

                jobNotificationService.Update(jobNotification);

                //return Ok();
                return NoContent();
            }
            catch (Exception)
            {
                return NotFound();
            }
        }
    }
}