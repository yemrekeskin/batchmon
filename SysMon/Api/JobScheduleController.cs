﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SysMon.Models;
using SysMon.Services;
using SysMon.ServiceViewModels;
using SysMon.ViewModels;

namespace SysMon.Api
{
    [Route("api/v1/jobschedule")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class JobScheduleController 
        : ControllerBase
    {
        private readonly ILogger<JobScheduleController> logger;
        private readonly IMapper mapper;
        private readonly IUserService userService;
        private readonly IJobScheduleService jobScheduleService;

        public JobScheduleController(
            ILogger<JobScheduleController> logger,
            IMapper mapper, 
            IUserService userService,
            IJobScheduleService jobScheduleService)
        {
            this.logger = logger;
            this.mapper = mapper;
            this.userService = userService;
            this.jobScheduleService = jobScheduleService;
        }


        [HttpGet]
        public IActionResult Get()
        {
            var currentUser = userService.GetUser();
            if (currentUser != null)
            {
                var userId = currentUser.Id;

                var list = jobScheduleService.List(d => d.UserId == userId);
                var result = mapper.Map<IEnumerable<JobScheduleViewModel>>(list);

                return Ok(result);
            }
            return NoContent();
        }

        [HttpGet("{id}", Name = "GetJobSchedule")]
        public IActionResult Get(long id)
        {
            var jobConfig = jobScheduleService.Get(id);
            if (jobConfig == null)
            {
                return NotFound();
            }
            var result = mapper.Map<JobScheduleViewModel>(jobConfig);
            return Ok(result);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            var jobSchedule = jobScheduleService.Get(id);
            if (jobSchedule == null)
            {
                return NotFound();
            }

            jobScheduleService.Remove(jobSchedule);
            return NoContent();
        }

        [HttpPost()]
        public IActionResult Post([FromBody] JobScheduleViewModel jobScheduleViewModel)
        {
            if (jobScheduleViewModel == null)
            {
                return BadRequest();
            }

            var jobSchedule = mapper.Map<JobSchedule>(jobScheduleViewModel);
            jobScheduleService.Add(jobSchedule);

            if (jobSchedule.Id.Equals(0))
            {
                return StatusCode(500, "Error");
            }

            var result = mapper.Map<JobScheduleViewModel>(jobSchedule);
            return CreatedAtRoute("GetJobSchedule", new { result.Id }, result);
        }

        [HttpPut("{id}")]
        public IActionResult Put(long id, [FromBody] JobScheduleViewModel jobScheduleViewModel)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            if (jobScheduleViewModel == null)
            {
                return BadRequest();
            }

            var jobScheule = jobScheduleService.Get(id);
            if (jobScheule == null)
            {
                return NotFound();
            }

            try
            {
                jobScheule.DateTime = jobScheduleViewModel.DateTime;
                jobScheduleService.Update(jobScheule);
                //return Ok();
                return NoContent();
            }
            catch (Exception)
            {
                return NotFound();
            }
        }
    }
}