﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SysMon.Models;
using SysMon.Services;
using SysMon.ServiceViewModels;
using SysMon.ViewModels;

namespace SysMon.Api
{
    [Route("api/v1/roles")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class RoleController 
        : ControllerBase
    {
        private readonly ILogger<RoleController> logger;
        private readonly IMapper mapper;
        private readonly IUserService userService;
        private readonly IRoleService roleService;

        public RoleController(
            ILogger<RoleController> logger,
            IMapper mapper,
            IUserService userService,
            IRoleService roleService)
        {
            this.logger = logger;
            this.mapper = mapper;
            this.userService = userService;
            this.roleService = roleService;
        }


        [HttpGet]
        public IActionResult Get()
        {
            var list = roleService.GetAllRoles(); 
            var result = mapper.Map<IEnumerable<RoleViewModel>>(list);
            return Ok(result);
        }
    }
}