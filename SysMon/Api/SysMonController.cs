﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SysMon.Models;
using SysMon.Services;
using SysMon.ServiceViewModels;
using SysMon.ViewModels;

namespace SysMon.Api
{
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class SysMonController 
        : Controller
    {
        private readonly ILogger<JobScheduleController> logger;
        private readonly IMapper mapper;
        private readonly IUserService userService;

        private readonly IGroupService groupService;
        private readonly IJobConfigService jobConfigService;        
        private readonly IJobScheduleService jobScheduleService;
        private readonly IJobService jobService;
        private readonly IWatcherService watcherService;

        public SysMonController(
            ILogger<JobScheduleController> logger,
            IMapper mapper,
            IUserService userService,
            IJobConfigService jobConfigService,
            IJobScheduleService jobScheduleService,
            IJobService jobService,
            IWatcherService watcherService,
            IGroupService groupService)
        {
            this.logger = logger;
            this.mapper = mapper;
            this.userService = userService;
            this.jobConfigService = jobConfigService;
            this.jobScheduleService = jobScheduleService;
            this.jobService = jobService;
            this.watcherService = watcherService;
            this.groupService = groupService;
        }

        [Route("api/v1/sysmon/projects")]
        [HttpGet]
        public IActionResult GetProjects()
        {
            var list = jobService.List();
            var result = mapper.Map<IEnumerable<JobViewModel>>(list);
            var projects = result.Select(d => d.ProjectName).Distinct();
            return Ok(projects);
        }

        [Route("api/v1/sysmon/watchers")]
        [HttpGet]
        public IActionResult GetWatcherList(string filter)
        {
            var watcherlist = watcherService.List().AsQueryable();
            var watchers = mapper.Map<IEnumerable<WatcherViewModel>>(watcherlist.ToList());
            foreach (var item in watchers)
            {
                var group = groupService.Get(item.GroupId);
                item.GroupName = group != null ? group.GroupName : String.Empty;
            }

            if (!String.IsNullOrEmpty(filter))
            {
                var filterBy = filter.Trim().ToLowerInvariant();
                if (!string.IsNullOrEmpty(filterBy))
                {
                    watchers = watchers
                           .Where(m => m.Name.ToLowerInvariant().Contains(filterBy) ||
                                       m.GroupName.ToLowerInvariant().Contains(filterBy) ||
                                       m.ManagerName.ToLowerInvariant().Contains(filterBy));
                }
            }

            return Ok(watchers);
        }

        [Route("api/v1/sysmon/jobs")]
        [HttpGet]
        public IActionResult GetJobList(string project, string filter)
        {   
            var joblist = jobService.List().AsQueryable();

            if(!string.IsNullOrEmpty(project))
            {
                joblist = joblist.Where(d => d.ProjectName == project);
            }

            if (!string.IsNullOrEmpty(filter))
            {
                var filterBy = filter.Trim().ToLowerInvariant();
                joblist = joblist.Where(m => m.JobName.ToLowerInvariant().Contains(filterBy));
            }

            var result = mapper.Map<IEnumerable<SearchJobViewModel>>(joblist.ToList());
            return Ok(result);
        }

        [Route("api/v1/sysmon/fulljobs")]
        [HttpGet]
        public IActionResult GetFullJobs()
        {
            // kullanıcıya tanımlı jobConfig Listesi
            var currentUser = userService.GetUser();
            if(currentUser != null)
            {
                var userId = currentUser.Id;
                var userJobConfigs = jobConfigService.List(d => d.UserId == userId);
                var userJobConfigsViewModel = mapper.Map<IEnumerable<JobConfigViewModel>>(userJobConfigs);

                var result = new GetJobViewModel();

                var jobs = new List<JobViewModel>();
                foreach (var userJobConfig in userJobConfigsViewModel)
                {
                    var jobId = userJobConfig.JobId;

                    var job = jobService.Get(jobId);
                    var jobViewModel = mapper.Map<JobViewModel>(job);

                    jobViewModel.NotifyOnFailure = userJobConfig.NotifyOnFailure;
                    jobViewModel.NotifyOnDelay = userJobConfig.NotifyOnDelay;
                    jobViewModel.ThresholdForDelay = userJobConfig.ThresholdForDelay;
                    jobViewModel.Hot = userJobConfig.Hot;
                    jobViewModel.JobRunId = jobViewModel.JobRunId;
                    jobViewModel.JobId = userJobConfig.JobId;
                    jobViewModel.JobConfigId = userJobConfig.Id;
                    jobs.Add(jobViewModel);
                }
                result.Jobs = jobs;

                var schedules = jobScheduleService.List(d => d.UserId == userId);
                var schedulesViewModel = mapper.Map<IEnumerable<JobScheduleViewModel>>(schedules);
                result.Schedules = schedulesViewModel.ToList();

                return Ok(result);
            }
            return Ok();
        }

        [Route("api/v1/sysmon/jobs")]
        [HttpPost]
        public IActionResult InsertJobs([FromBody] PostJobViewModel postJobViewModel)
        {
            var currentUser = userService.GetUser();
            if (currentUser != null)
            {
                var userId = currentUser.Id;

                jobScheduleService.Remove(d => d.UserId == userId);
                List<JobScheduleViewModel> jobScheduleViewModels = postJobViewModel.Schedules;
                var jobSchedules = mapper.Map<IEnumerable<JobSchedule>>(jobScheduleViewModels).ToList();
                foreach (var item in jobSchedules)
                {
                    item.UserId = userId;
                }
                jobScheduleService.AddBatch(jobSchedules);

                jobConfigService.Remove(d => d.UserId == userId);
                List<JobConfigViewModel> jobConfigViewModels = postJobViewModel.JobConfigs;
                var jobConfigs = mapper.Map<IEnumerable<JobConfig>>(jobConfigViewModels).ToList();
                foreach (var item in jobConfigs)
                {
                    item.UserId = userId;
                }
                jobConfigService.AddBatch(jobConfigs);
            }
            return Ok();
        }

    }
}