﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SysMon.Models;
using SysMon.Services;
using SysMon.ServiceViewModels;
using SysMon.ViewModels;

namespace SysMon.Api
{
    [Route("api/v1/users")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class UserController 
        : ControllerBase
    {
        private readonly ILogger<UserController> logger;
        private readonly IMapper mapper;
        private readonly IUserService userService;
        private readonly IRoleService roleService;

        public UserController(
            ILogger<UserController> logger,
            IMapper mapper,
            IUserService userService,
            IRoleService roleService)
        {
            this.logger = logger;
            this.mapper = mapper;
            this.userService = userService;
            this.roleService = roleService;
        }


        [HttpGet]
        public IActionResult Get()
        {
            var list = userService.GetAllUsers();
            var result = mapper.Map<IEnumerable<UserViewModel>>(list);
            return Ok(result);
        }

        [HttpGet("{id}", Name = "GetUser")]
        public IActionResult Get(int id)
        {
            var user = userService.GetUserById(id.ToString());
            if (user == null)
            {
                return NotFound();
            }
            var result = mapper.Map<UserViewModel>(user);
            return Ok(result);
        }
        
        [HttpPost()]
        public IActionResult Post([FromBody] UserViewModel userViewModel)
        {
            if (userViewModel == null)
            {
                return BadRequest();
            }

            var user = mapper.Map<User>(userViewModel);
            var userCreationResult = userService.Create(user);
            if(userCreationResult.Succeeded)
            {
                string roleId = userViewModel.RoleId;
                if(!String.IsNullOrEmpty(roleId))
                {
                    Role role = roleService.GetRoleById(roleId);
                    userService.AddUserToRole(user, role.Name);
                }
            }

            if (user.Id.Equals(0))
            {
                return StatusCode(500, "Error");
            }

            var result = mapper.Map<UserViewModel>(user);
            return CreatedAtRoute("GetUser", new { result.Id }, result);
        }
    }
}