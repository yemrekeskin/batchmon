﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SysMon.Models;
using SysMon.Services;
using SysMon.ServiceViewModels;
using SysMon.ViewModels;

namespace SysMon.Api
{
    [Route("api/v1/watchers")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class WatcherController 
        : ControllerBase
    {
        private readonly ILogger<WatcherController> logger;
        private readonly IMapper mapper;
        private readonly IUserService userService;
        private readonly IWatcherService watcherService;
        private readonly IGroupService groupService;

        public WatcherController(
            ILogger<WatcherController> logger,
            IMapper mapper,
            IUserService userService,
            IWatcherService watcherService,
            IGroupService groupService)
        {
            this.logger = logger;
            this.mapper = mapper;
            this.userService = userService;
            this.watcherService = watcherService;
            this.groupService = groupService;
        }


        [HttpGet]
        public IActionResult Get()
        {
            var list = watcherService.List();
            var result = mapper.Map<IEnumerable<WatcherViewModel>>(list);
            foreach (var item in result)
            {
                var group = groupService.Get(item.GroupId);
                item.GroupName = group != null ? group.GroupName : String.Empty;
            }
            return Ok(result);
        }

        [HttpGet("{id}", Name = "GetWatcher")]
        public IActionResult Get(long id)
        {
            var watcher = watcherService.Get(id);
            if (watcher == null)
            {
                return NotFound();
            }
            var result = mapper.Map<WatcherViewModel>(watcher);
            return Ok(result);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            var watcher = watcherService.Get(id);
            if (watcher == null)
            {
                return NotFound();
            }

            watcherService.Remove(watcher);
            return NoContent();
        }

        [HttpPost()]
        public IActionResult Post([FromBody] WatcherViewModel watcherViewModel)
        {
            if (watcherViewModel == null)
            {
                return BadRequest();
            }

            var watcher = mapper.Map<Watcher>(watcherViewModel);
            watcherService.Add(watcher);

            if (watcher.Id.Equals(0))
            {
                return StatusCode(500, "Error");
            }

            var result = mapper.Map<WatcherViewModel>(watcher);
            return CreatedAtRoute("GetWatcher", new { result.Id }, result);
        }

        [HttpPut("{id}")]
        public IActionResult Put(long id, [FromBody] WatcherViewModel watcherViewModel)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            if (watcherViewModel == null)
            {
                return BadRequest();
            }

            var watcher = watcherService.Get(id);
            if (watcher == null)
            {
                return NotFound();
            }

            try
            {
                watcher.Email = watcherViewModel.Email;
                watcher.Name = watcherViewModel.Name;
                watcher.PhoneNumber = watcherViewModel.PhoneNumber;

                watcherService.Update(watcher);

                //return Ok();
                return NoContent();
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // For Pages

        [HttpPost]
        [Route("Delete")]
        public IActionResult Delete([FromBody] WatcherViewModel watcherViewModel)
        {
            var watcher = watcherService.Get(watcherViewModel.Id);
            if (watcher == null)
            {
                return NotFound();
            }

            watcherService.Remove(watcher);
            return NoContent();
        }

        [HttpPost]
        [Route("Update")]
        public IActionResult Put([FromBody] WatcherViewModel watcherViewModel)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            if (watcherViewModel == null)
            {
                return BadRequest();
            }

            var watcher = watcherService.Get(watcherViewModel.Id);
            if (watcher == null)
            {
                return NotFound();
            }

            try
            {
                watcher.Email = watcherViewModel.Email;
                watcher.Name = watcherViewModel.Name;
                watcher.PhoneNumber = watcherViewModel.PhoneNumber;

                watcherService.Update(watcher);

                //return Ok();
                return NoContent();
            }
            catch (Exception)
            {
                return NotFound();
            }
        }
    }
}