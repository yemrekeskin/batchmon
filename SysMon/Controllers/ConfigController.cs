﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SysMon.PageViewModels;
using SysMon.Services;
using Microsoft.AspNetCore.Authorization;

namespace SysMon.Controllers
{
    [Authorize]
    public class ConfigController 
        : Controller
    {
        private readonly IConfigService configService;

        public ConfigController(
            IConfigService configService)
        {
            this.configService = configService;
        }

        public IActionResult Index()
        {
            var configs = configService.List();

            
            var SmtpServer = configs.Single(d => d.Key == "SMTP_SERVER");
            var SmtpPort = configs.Single(d => d.Key == "SMTP_PORT");
            var SmtpUserName = configs.Single(d => d.Key == "SMTP_USERNAME");
            var SmtpPassword = configs.Single(d => d.Key == "SMTP_PASSWORD");
            var Subject = configs.Single(d => d.Key == "SUBJECT");
            var AlertRepetion = configs.Single(d => d.Key == "ALERT_REPETITION");
            var AlertRepetitionRange = configs.Single(d => d.Key == "ALERT_REPETITION_RANGE");


            ConfigItem configItem = new ConfigItem();
            configItem.SmtpServer = SmtpServer.Value;
            configItem.SmtpUserName = SmtpUserName.Value;
            configItem.SmtpPort = SmtpPort.Value;
            configItem.Subject = Subject.Value;
            configItem.SmtpPassword = SmtpPassword.Value;
            configItem.AlertRepetition = int.Parse(AlertRepetion.Value);
            configItem.AlertRepetitionRange = int.Parse(AlertRepetitionRange.Value);

            return View(configItem);
        }

        [HttpPost]
        public IActionResult Index(ConfigItem configItem)
        {
            var configs = configService.List();

            var SmtpServer = configs.Single(d => d.Key == "SMTP_SERVER");
            if(SmtpServer.Value != configItem.SmtpServer)
            {
                SmtpServer.Value = configItem.SmtpServer;
                configService.Update(SmtpServer);
            }
            
            var SmtpPort = configs.Single(d => d.Key == "SMTP_PORT");
            if(SmtpPort.Value != configItem.SmtpPort)
            {
                SmtpPort.Value = configItem.SmtpPort;
                configService.Update(SmtpPort);
            }
               
            var SmtpUserName = configs.Single(d => d.Key == "SMTP_USERNAME");
            if(SmtpUserName.Value != configItem.SmtpUserName)
            {
                SmtpUserName.Value = configItem.SmtpUserName;
                configService.Update(SmtpUserName);
            }
            
            var SmtpPassword = configs.Single(d => d.Key == "SMTP_PASSWORD");
            if(!String.IsNullOrEmpty(configItem.SmtpPassword) && SmtpPassword.Value != configItem.SmtpPassword)
            {
                SmtpPassword.Value = configItem.SmtpPassword;
                configService.Update(SmtpPassword);
            }
            
            var Subject = configs.Single(d => d.Key == "SUBJECT");
            if(Subject.Value != configItem.Subject)
            {
                Subject.Value = configItem.Subject;
                configService.Update(Subject);
            }

            var AlertRepetition = configs.Single(d => d.Key == "ALERT_REPETITION");
            if (int.Parse(AlertRepetition.Value) != configItem.AlertRepetition)
            {
                AlertRepetition.Value = configItem.AlertRepetition.ToString();
                configService.Update(AlertRepetition);
            }

            var AlertRepetitionRange = configs.Single(d => d.Key == "ALERT_REPETITION_RANGE");
            if (int.Parse(AlertRepetitionRange.Value) != configItem.AlertRepetitionRange)
            {
                AlertRepetitionRange.Value = configItem.Subject;
                configService.Update(AlertRepetitionRange);
            }

            return View();
        }
    }
}