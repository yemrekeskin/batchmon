﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SysMon.Models;
using SysMon.PageViewModels;
using SysMon.Services;
using Microsoft.AspNetCore.Authorization;

namespace SysMon.Controllers
{
    [Authorize]
    public class GroupController 
        : Controller
    {
        private readonly IGroupService groupService;
        public GroupController(
            IGroupService groupService)
        {
            this.groupService = groupService;
        }

        public IActionResult Index()
        {
            var groups = groupService.List();

            List<GroupItem> groupItems = new List<GroupItem>();
            foreach (var group in groups)
            {
                GroupItem groupItem = new GroupItem();
                groupItem.GroupName = group.GroupName;
                groupItem.Explanation = group.Explanation;
                groupItem.Id = group.Id;
                groupItems.Add(groupItem);
            }

            return View(groupItems);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(GroupItem groupItem)
        {
            try
            {
                var group = new Group();
                group.GroupName = groupItem.GroupName;
                group.Explanation = groupItem.Explanation;
                groupService.Add(group);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Error while group create");
                return View(groupItem);
            }
        }

        public IActionResult Edit(int id)
        {
            var group = groupService.Get(id);
            var groupItem = new GroupItem();
            groupItem.GroupName = group.GroupName;
            groupItem.Explanation = group.Explanation;
            return View(groupItem);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, GroupItem groupItem)
        {
            try
            {
                var group = groupService.Get(id);
                group.GroupName = groupItem.GroupName;
                group.Explanation = groupItem.Explanation;
                groupService.Update(group);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Error while group update");
                return View(groupItem);
            }
        }

        public IActionResult Remove(int id) 
        {
            try
            {
                groupService.Remove(id);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Error while group delete");
                return RedirectToAction(nameof(Index));
            }
        }
    }
}