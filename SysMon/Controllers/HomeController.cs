﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SysMon.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using SysMon.ViewModels;
using SysMon.Services;

namespace SysMon.Controllers
{
    //[Authorize(Roles = "Admin,SuperAdmin,SysmonUser,SuperUser")]
    [Authorize]
    public class HomeController 
        : Controller
    {
        private readonly INotificationService notificationService;
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<Role> _roleManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IPasswordHasher<User> passwordHasher;

        public HomeController(
            INotificationService notificationService,
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            RoleManager<Role> roleManager,
            IPasswordHasher<User> passwordHasher)
        {
            this.notificationService = notificationService;
            this._userManager = userManager;
            this._signInManager = signInManager;
            this.passwordHasher = passwordHasher;
            this._roleManager = roleManager;
        }

        public async Task<IActionResult> Index()
        {

            //// **************** PUSH

            //var deviceId = "fObnLTwqJyI:APA91bE6YH1yj5p1vaGYLaAhjS0ywJbNwSyuef1_UyKkYHmI-VVcK8F2wMNEzBSfnZkQTtnmzyGlewqYnzK4klMJmYN5RrBL0mmnfVLi1LR-b8_DEjaZL1OWfbOOHAeUfTacvBu6D926";
            //var notificationMessage = new NotificationMessage();
            //notificationMessage.DeviceId = deviceId;
            //notificationMessage.DeviceType = DeviceType.ANDROID;
            //notificationMessage.Message = "Hello SysmonUser";
            //notificationMessage.Sound = "ringtone1.mp3";
            //notificationService.Push(notificationMessage);

            //// **************** ROLES

            //var role1 = new Role();
            //role1.Name = "SystemAdmin";
            //role1.Explanation = "System Admin Explanation";
            //var result1 = _roleManager.CreateAsync(role1).Result;

            //var role2 = new Role();
            //role2.Name = "Manager";
            //role2.Explanation = "Manager Explanation";
            //_roleManager.CreateAsync(role2);

            //var role3 = new Role();
            //role3.Name = "SysmonUser";
            //role3.Explanation = "SysmonUser Explanation";
            //var result3 = _roleManager.CreateAsync(role3).Result;

            //var role4 = new Role();
            //role4.Name = "SuperUser";
            //role4.Explanation = "SuperUser Explanation";
            //_roleManager.CreateAsync(role4);


            //// **************** USERS

            //var user = new User();
            //user.Email = "systemadmin2@msysmon.com";
            //user.EmailConfirmed = true;
            //user.UserName = "systemadmin2";
            //user.PasswordHash = passwordHasher.HashPassword(user, "1Qazxsw2.0");
            //var creationUser = _userManager.CreateAsync(user);
            //if (creationUser.Result.Succeeded)
            //{
            //    return View();
            //}

            //var user0 = _userManager.FindByNameAsync("sysmonuser").Result;
            //var result0 = _userManager.AddToRoleAsync(user0, "SysmonUser").Result;

            //var user = _userManager.FindByNameAsync("sysmonuser2").Result;
            //var result1 = _userManager.AddToRoleAsync(user, "SysmonUser").Result;

            //var user1 = _userManager.FindByNameAsync("superadmin").Result;
            //await _userManager.AddToRoleAsync(user1, "SuperAdmin");

            //var user2 = _userManager.FindByNameAsync("superuser").Result;
            //await _userManager.AddToRoleAsync(user2, "SuperUser");

            //var user3 = _userManager.FindByNameAsync("sysmonuser").Result;
            //await _userManager.AddToRoleAsync(user3, "SysmonUser");

            //var user1 = new User();
            //user1.Email = "admin@sysmon.com";
            //user1.EmailConfirmed = true;
            //user1.UserName = "admin";
            //user1.PasswordHash = passwordHasher.HashPassword(user1, "1Qazxsw2.0");
            //var creationUser1 = _userManager.CreateAsync(user1);
            //if (creationUser1.Result.Succeeded)
            //{
            //    return View();
            //}

            //var user2 = new User();
            //user2.Email = "manager2@msysmon.com";
            //user2.EmailConfirmed = true;
            //user2.UserName = "manager2";
            //user2.PasswordHash = passwordHasher.HashPassword(user2, "1Qazxsw2.0");
            //var creationUser2 = _userManager.CreateAsync(user2);
            //if (creationUser2.Result.Succeeded)
            //{
            //    //return View();
            //}

            //var user3 = new User();
            //user3.Email = "sysmonuser2@msysmon.com";
            //user3.EmailConfirmed = true;
            //user3.UserName = "sysmonuser2";
            //user3.PasswordHash = passwordHasher.HashPassword(user3, "1Qazxsw2.0");
            //var creationUser3 = _userManager.CreateAsync(user3);
            //if (creationUser3.Result.Succeeded)
            //{
            //    //return View();
            //}

            //var pass = "1Qazxsw2.0";
            //var userName = "yemrekeskin";
            //var signinResult = await _signInManager.PasswordSignInAsync(
            //        userName,
            //        pass,
            //        false, false);

            //if (signinResult.Succeeded)
            //{
            //    return View();
            //}

            //var user = await _userManager.FindByNameAsync(userName);

            //if (signinResult.IsNotAllowed)
            //{
            //    if (!await _userManager.IsEmailConfirmedAsync(user))
            //    {
            //        // Email isn't confirmed.
            //    }

            //    if (!await _userManager.IsPhoneNumberConfirmedAsync(user))
            //    {
            //        // Phone Number isn't confirmed.
            //    }
            //}
            //else if (signinResult.IsLockedOut)
            //{
            //    // Account is locked out.
            //}
            //else if (signinResult.RequiresTwoFactor)
            //{
            //    // 2FA required.
            //}
            //else
            //{
            //    // Username or password is incorrect.
            //    if (user == null)
            //    {
            //        // Username is incorrect.
            //    }
            //    else
            //    {
            //        // Password is incorrect.
            //    }
            //}

            return View();
        }

        [Authorize]
        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
