﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SysMon.Models;
using SysMon.PageViewModels;
using SysMon.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;

namespace SysMon.Controllers
{
    [Authorize]
    public class JobController 
        : Controller
    {
        private readonly IUserService userService;
        private readonly IJobService jobService;
        private readonly IJobConfigService jobConfigService;
        private readonly UserManager<User> userManager;

        public JobController(
            IUserService userService,
            IJobService jobService,
            IJobConfigService jobConfigService,
            UserManager<User> userManager)
        {
            this.userService = userService;
            this.jobService = jobService;
            this.jobConfigService = jobConfigService;
            this.userManager = userManager;
        }

        public IActionResult Index()
        {
            var currentUser = userService.GetUser();
            //var currentUser = userService.GetUserById("1");

            var userJobConfigs = new List<JobConfig>();
            if (currentUser != null)
            {
                var userId = currentUser.Id;
                userJobConfigs = jobConfigService.List(d => d.UserId == userId).ToList();
            }

            var jobs = jobService.List();

            List<JobItem> jobItems = new List<JobItem>();
            foreach (var job in jobs)
            {
                JobItem jobItem = new JobItem();
                jobItem.Delay = job.Delay;
                jobItem.EndTime = job.EndTime;
                jobItem.Id = job.Id;
                jobItem.JobDetail = job.JobDetail;
                jobItem.JobName = job.JobName;
                jobItem.JobRunId = job.JobRunId;
                jobItem.JobState = job.JobState.ToString();
                jobItem.ProjectName = job.ProjectName;
                jobItem.StartTime = job.StartTime;

                if(userJobConfigs.Count > 0)
                {
                    var userJobConfig = userJobConfigs.SingleOrDefault(d => d.JobId == job.Id);
                    jobItem.IsOwned = userJobConfig != null ? true : false;
                }
                else
                {
                    jobItem.IsOwned = false;
                }

                jobItems.Add(jobItem);
            }
            return View(jobItems);
        }

        public IActionResult MyJob()
        {
            // kullanıcıya tanımlı jobConfig Listesi
            var currentUser = userService.GetUser();
            //var currentUser = userService.GetUserById("1");
            if (currentUser != null)
            {
                var userId = currentUser.Id;
                var userJobConfigs = jobConfigService.List(d => d.UserId == userId);

                if(User.IsInRole("Manager"))
                {
                    
                }

                List<MyJobItem> jobItems = new List<MyJobItem>();
                foreach (var userJobConfig in userJobConfigs)
                {
                    var jobId = userJobConfig.JobId;
                    var job = jobService.Get(jobId);

                    MyJobItem jobItem = new MyJobItem();
                    jobItem.Delay = job.Delay;
                    jobItem.EndTime = job.EndTime;
                    jobItem.Id = job.Id;
                    jobItem.JobDetail = job.JobDetail;
                    jobItem.JobName = job.JobName;
                    jobItem.JobRunId = job.JobRunId;
                    jobItem.JobState = job.JobState.ToString();
                    jobItem.ProjectName = job.ProjectName;
                    jobItem.StartTime = job.StartTime;
                    // config detils
                    jobItem.JobConfigId = userJobConfig.Id;
                    jobItem.NotifyOnDelay = userJobConfig.NotifyOnDelay;
                    jobItem.NotifyOnFailure = userJobConfig.NotifyOnFailure;
                    jobItem.Hot = userJobConfig.Hot;
                    jobItem.ThresholdForDelay = userJobConfig.ThresholdForDelay;
                    jobItems.Add(jobItem);
                }
                return View(jobItems);
            }

            return View(new List<MyJobItem>());
        }

        public IActionResult MyGroupJob()
        {
            // kullanıcıya tanımlı jobConfig Listesi
            var currentUser = userService.GetUser();
            //var currentUser = userService.GetUserById("1");
            if (currentUser != null)
            {
                var userId = currentUser.Id;
                var groupId = currentUser.GroupId;

                var users = userManager.Users.Where(d => d.GroupId == groupId && d.Id != userId);


                List<MyJobItem> jobItems = new List<MyJobItem>();

                foreach (var user in users)
                {
                    var userJobConfigs = jobConfigService.List(d => d.UserId == user.Id);

                    foreach (var userJobConfig in userJobConfigs)
                    {
                        var jobId = userJobConfig.JobId;
                        var job = jobService.Get(jobId);

                        MyJobItem jobItem = new MyJobItem();
                        jobItem.UserName = user.UserName;
                        jobItem.Delay = job.Delay;
                        jobItem.EndTime = job.EndTime;
                        jobItem.Id = job.Id;
                        jobItem.JobDetail = job.JobDetail;
                        jobItem.JobName = job.JobName;
                        jobItem.JobRunId = job.JobRunId;
                        jobItem.JobState = job.JobState.ToString();
                        jobItem.ProjectName = job.ProjectName;
                        jobItem.StartTime = job.StartTime;
                        // config detils
                        jobItem.JobConfigId = userJobConfig.Id;
                        jobItem.NotifyOnDelay = userJobConfig.NotifyOnDelay;
                        jobItem.NotifyOnFailure = userJobConfig.NotifyOnFailure;
                        jobItem.Hot = userJobConfig.Hot;
                        jobItem.ThresholdForDelay = userJobConfig.ThresholdForDelay;
                        jobItems.Add(jobItem);
                    }
                }
                return View(jobItems);
            }

            return View(new List<MyJobItem>());
        }

        public IActionResult Add(int id)
        {
            var currentUser = userService.GetUser();
            //var currentUser = userService.GetUserById("1");
            if (currentUser != null)
            {
                var jobId = id;
                var userId = currentUser.Id;

                JobConfig jobConfig = new JobConfig();
                jobConfig.UserId = userId;
                jobConfig.JobId = jobId;
                jobConfigService.Add(jobConfig);
            }
            return RedirectToAction(nameof(Index));
        }

        public IActionResult Remove(int id)
        {
            var currentUser = userService.GetUser();
            //var currentUser = userService.GetUserById("1");
            if (currentUser != null)
            {
                var jobId = id;
                var userId = currentUser.Id;

                var userJobConfigs = jobConfigService.List(d => d.UserId == userId);
                var userJobConfig = userJobConfigs.SingleOrDefault(d => d.JobId == jobId);
                if(userJobConfig != null)
                {
                    jobConfigService.Remove(userJobConfig.Id);
                }
            }
            return RedirectToAction(nameof(Index));
        }

        public IActionResult Config(int id)
        {
            // kullanıcıya tanımlı jobConfig Listesi
            var currentUser = userService.GetUser();
            //var currentUser = userService.GetUserById("1");
            if (currentUser != null)
            {
                var userId = currentUser.Id;
                var userJobConfig = jobConfigService.Get(id);
                var job = jobService.Get(userJobConfig.JobId);

                if (userJobConfig != null)
                {
                    var userJobConfigItem = new JobConfigItem();
                    userJobConfigItem.Id = userJobConfig.Id;
                    userJobConfigItem.Hot = userJobConfig.Hot;
                    userJobConfigItem.NotifyOnDelay = userJobConfig.NotifyOnDelay;
                    userJobConfigItem.NotifyOnFailure = userJobConfig.NotifyOnFailure;
                    userJobConfigItem.ThresholdForDelay = userJobConfig.ThresholdForDelay;
                    userJobConfigItem.JobName = job.JobName;
                    userJobConfigItem.JobDetail = job.JobDetail;

                    return View(userJobConfigItem);
                }
                return RedirectToAction(nameof(MyJob));
            }
            return RedirectToAction(nameof(MyJob));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Config(int id, JobConfigItem jobConfigItem)
        {
            try
            {
                var userJobConfig = jobConfigService.Get(id);
                userJobConfig.Hot = jobConfigItem.Hot;
                userJobConfig.NotifyOnDelay = jobConfigItem.NotifyOnDelay;
                userJobConfig.NotifyOnFailure = jobConfigItem.NotifyOnFailure;
                userJobConfig.ThresholdForDelay = jobConfigItem.ThresholdForDelay;

                jobConfigService.Update(userJobConfig);

                return RedirectToAction(nameof(MyJob));
            }
            catch
            {
                return View();
            }
        }
    }
}