﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SysMon.ManageViewModels;
using SysMon.PageViewModels;
using SysMon.Services;
using SysMon.ViewModels;
using SysMon;
using SysMon.Models;
using Microsoft.AspNetCore.Http;

namespace SysMon.Controllers 
{
    [Authorize]
    public class ManageController
        : Controller
    {
        private readonly UserManager<User> userManager;
        private readonly RoleManager<Role> roleManager;
        private readonly SignInManager<User> signInManager;
        private readonly IGroupService groupService;
        private readonly ILogger logger;
        protected readonly IHttpContextAccessor httpContextAccessor;

        public ManageController(
            UserManager<User> userManager,
            RoleManager<Role> roleManager,
            SignInManager<User> signInManager,
            IGroupService groupService,
            IHttpContextAccessor httpContextAccessor,
            ILoggerFactory loggerFactory)
        {
            this.userManager = userManager;
            this.roleManager = roleManager;
            this.signInManager = signInManager;
            this.groupService = groupService;
            this.httpContextAccessor = httpContextAccessor;
            this.logger = loggerFactory.CreateLogger<AccountController>();
        }

        [HttpGet]
        public IActionResult Index()
        {
            var currentUser = userManager.GetUserAsync(httpContextAccessor.HttpContext.User).Result;
            if (currentUser != null)
            {
                var roleOptions = roleManager.Roles.Select(d => new KeyValue()
                {
                    Key = d.Id,
                    Value = d.Name
                });
                ViewBag.ListOfRole = roleOptions;

                var groupOptions = groupService.List().Select(d => new KeyValue()
                {
                    Key = d.Id.ToString(),
                    Value = d.GroupName
                });
                ViewBag.ListOfGroup = groupOptions;

                var userId = currentUser.Id;
                var profile = new ProfileItem();
                profile.Email = currentUser.Email;
                profile.UserName = currentUser.UserName;
                profile.Name = currentUser.Name;
                profile.Surname = currentUser.Surname;

                var groups = groupService.List();
                profile.GroupId = currentUser.GroupId;
                if (currentUser.GroupId != 0)
                {
                    var group = groups.Single(d => d.Id == currentUser.GroupId);
                    profile.GroupName = group != null ? group.GroupName : String.Empty;
                }

                var userRoles = userManager.GetRolesAsync(currentUser).Result;
                var userRoleName = userRoles.FirstOrDefault();
                if (userRoles.Count > 0 && userRoleName != null)
                {
                    Role userRole = roleManager.Roles.Where(d => d.Name == userRoleName).SingleOrDefault(); 

                    profile.RoleId = userRole != null ? userRole.Id : String.Empty;
                    profile.RoleName = userRole != null ? userRole.Name : String.Empty;
                }

                return View(profile);
            }
            return RedirectToAction("Error");
        }

        [HttpPost]
        public IActionResult Index(ProfileItem profileItem)
        {
            var currentUser = userManager.GetUserAsync(httpContextAccessor.HttpContext.User).Result;
            //var currentUser = userService.GetUserById("1");
            if (currentUser != null)
            {
                currentUser.UserName = profileItem.UserName;
                currentUser.GroupId = profileItem.GroupId;
                currentUser.Name = profileItem.Name;
                currentUser.Surname = profileItem.Surname;

                userManager.UpdateAsync(currentUser);

                //var currentRole = userManager.GetRolesAsync(currentUser).Result.SingleOrDefault();

                //var updatedRoleId = profileItem.RoleId;
                //var updatedRole = roleManager.FindByIdAsync(updatedRoleId.ToString()).Result;
                //if (!String.IsNullOrEmpty(updatedRoleId) && updatedRole != null)
                //{
                //    userManager.RemoveFromRoleAsync(currentUser, currentRole);
                //    var current = userManager.FindByIdAsync(currentUser.Id.ToString()).Result;
                //    var result = userManager.AddToRoleAsync(current, updatedRole.Name);
                //}
            }
            return RedirectToAction("Index");
        }

        //
        // GET: /Manage/ChangePassword
        [HttpGet]
        public IActionResult ChangePassword()
        {
            return View();
        }

        //
        // POST: /Manage/ChangePassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var currentUser = userManager.GetUserAsync(httpContextAccessor.HttpContext.User).Result;
            if (currentUser != null)
            {
                var result = userManager.ChangePasswordAsync(currentUser, model.OldPassword, model.NewPassword).Result;
                if (result.Succeeded)
                {
                    //userService.SignIn(currentUser.Email, currentUser.Password);
                    logger.LogInformation(3, "User changed their password successfully.");
                    return RedirectToAction(nameof(Index), new { Message = ManageMessageId.ChangePasswordSuccess });
                }
                AddErrors(result);
                return View(model);
            }
            return RedirectToAction(nameof(Index), new { Message = ManageMessageId.Error });
        }


        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        public enum ManageMessageId
        {
            AddPhoneSuccess,
            AddLoginSuccess,
            ChangePasswordSuccess,
            SetTwoFactorSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            RemovePhoneSuccess,
            Error
        }

        private User GetCurrentUser()
        {
            return userManager.GetUserAsync(httpContextAccessor.HttpContext.User).Result;
        }
        
        #endregion
    }
}