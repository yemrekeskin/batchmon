﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using SysMon.Extensions;
using SysMon.Models;
using SysMon.PageViewModels;
using SysMon.Services;
using SysMon.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;

namespace SysMon.Controllers
{
    [Authorize]
    public class UserController 
        : Controller
    {
        private readonly IUserService userService;
        private readonly IRoleService roleService;
        private readonly IGroupService groupService;
        private readonly IMessagingService messagingService;
        private readonly IPasswordHasher<User> passwordHasher;

        public UserController(
            IUserService userService,
            IRoleService roleService,
            IGroupService groupService,
            IMessagingService messagingService,
            IPasswordHasher<User> passwordHasher)
        {
            this.userService = userService;
            this.roleService = roleService;
            this.groupService = groupService;
            this.messagingService = messagingService;
            this.passwordHasher = passwordHasher;
        }

        public IActionResult Index()
        {
            var users = userService.GetAllUsers();
            var groups = groupService.List();

            List<UserItem> userItems = new List<UserItem>();
            foreach (var user in users) 
            {
                UserItem userItem = new UserItem();
                userItem.Id = user.Id;
                userItem.Name = user.Name;
                userItem.Surname = user.Surname;
                userItem.UserName = user.UserName;
                userItem.Email = user.Email;
                userItem.EmailConfirmed = user.EmailConfirmed;

                userItem.GroupId = user.GroupId;
                if(user.GroupId != 0)
                {
                    var group = groups.SingleOrDefault(d => d.Id == user.GroupId);
                    userItem.GroupName = group != null ? group.GroupName : String.Empty;
                }                

                var userRoles = userService.GetUserRoles(user);
                var userRole = userRoles.FirstOrDefault();
                if(userRoles.Count > 0 && userRole != null)
                {
                    userItem.RoleId = userRole != null ? userRole.Id : String.Empty;
                    userItem.RoleName = userRole != null ? userRole.Name : String.Empty;
                }                

                userItems.Add(userItem);
            }
            return View(userItems);
        }

        public ActionResult Create()
        {
            var roles = roleService.GetAllRoles().Select(d => new KeyValue()
            {
                Key = d.Id,
                Value = d.Name
            });
            ViewBag.ListOfRole = roles;

            var groups = groupService.List().Select(d => new KeyValue()
            {
                Key = d.Id.ToString(),
                Value = d.GroupName
            });
            ViewBag.ListOfGroup = groups;

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(UserItem userItem)
        {
            try
            {
                User user = new User();
                user.Name = userItem.Name;
                user.Surname = userItem.Surname;
                user.UserName = userItem.UserName;
                user.Email = userItem.Email;
                user.GroupId = userItem.GroupId;
                user.EmailConfirmed = false;

                var pass = PasswordGenerator.Create(8);
                var result = userService.Create(user,pass);
                if(result.Succeeded == true)
                {
                    // add to role
                    if(!String.IsNullOrEmpty(userItem.RoleId))
                    {
                        var userRole = roleService.GetAllRoles().Single(d => d.Id == userItem.RoleId);
                        userService.AddUserToRole(user, userRole.Name);
                    }   
                    
                    // send mail
                    var emailMessage = new EmailMessage();
                    emailMessage.To.Add(userItem.Email);
                    emailMessage.Subject = "Welcome To Sysmon";
                    emailMessage.Content = "Your password :" + pass;
                    var emailResult = messagingService.SendMail(emailMessage);
                    if(emailResult.Succeed)
                    {
                        var userToUpdate = userService.GetUserByName(user.UserName);
                        userToUpdate.EmailConfirmed = true;
                        userService.Update(userToUpdate);
                    }
                }
                else
                {
                    AddErrors(result);
                }

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        public IActionResult Edit(string id)
        {
            var groups = groupService.List();
            var user = userService.GetUserById(id);
            if(user != null)
            {
                UserItem userItem = new UserItem();
                userItem.Id = user.Id;
                userItem.Name = user.Name;
                userItem.Surname = user.Surname;
                userItem.UserName = user.UserName;
                userItem.Email = user.Email;

                userItem.GroupId = user.GroupId;
                if (user.GroupId != 0)
                {
                    var group = groups.SingleOrDefault(d => d.Id == user.GroupId);
                    userItem.GroupName = group != null ? group.GroupName : String.Empty;
                }

                var userRoles = userService.GetUserRoles(user);
                var userRole = userRoles.FirstOrDefault();
                if (userRoles.Count > 0 && userRole != null)
                {
                    userItem.RoleId = userRole != null ? userRole.Id : String.Empty;
                    userItem.RoleName = userRole != null ? userRole.Name : String.Empty;
                }


                var roleOptions = roleService.GetAllRoles().Select(d => new KeyValue()
                {
                    Key = d.Id,
                    Value = d.Name
                });
                ViewBag.ListOfRole = roleOptions;

                var groupOptions = groupService.List().Select(d => new KeyValue()
                {
                    Key = d.Id.ToString(),
                    Value = d.GroupName
                });
                ViewBag.ListOfGroup = groupOptions;

                return View(userItem);
            }
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(string id, UserItem userItem)
        {
            try
            {
                var user = userService.GetUserById(id);

                bool isChanged = false;

                if(user.Name != userItem.Name)
                {
                    isChanged = true;
                    user.Name = userItem.Name;
                }
                if(user.Surname != userItem.Surname)
                {
                    isChanged = true;
                    user.Surname = userItem.Surname;
                }
                if(user.UserName != userItem.UserName)
                {
                    isChanged = true;
                    user.UserName = userItem.UserName;
                }                
                //user.Email = userItem.Email;
                if(user.GroupId != userItem.GroupId)
                {
                    isChanged = true;
                    user.GroupId = userItem.GroupId;
                }
                
                if(isChanged)
                {
                    userService.Update(user);
                }                

                if(String.IsNullOrEmpty(userItem.RoleName))
                {
                    var role = roleService.GetRoleById(userItem.RoleId);
                    userItem.RoleName = role.Name;
                }

                var currentRole = userService.GetUserRoles(user).SingleOrDefault();
                if (currentRole != null)
                {
                    if (currentRole.Name != userItem.RoleName)
                    {
                        userService.RemoveUserFromRole(user, currentRole.Name);
                        userService.AddUserToRole(user, userItem.RoleName);
                    }
                }
                else
                {
                    var role = roleService.GetRoleById(userItem.RoleId);
                    userService.AddUserToRole(user, role.Name);
                }

                var roleOptions = roleService.GetAllRoles().Select(d => new KeyValue()
                {
                    Key = d.Id,
                    Value = d.Name
                });
                ViewBag.ListOfRole = roleOptions;

                var groupOptions = groupService.List().Select(d => new KeyValue()
                {
                    Key = d.Id.ToString(),
                    Value = d.GroupName
                });
                ViewBag.ListOfGroup = groupOptions;

                return RedirectToAction(nameof(Index));
            }
            catch (Exception)
            {
                ModelState.AddModelError(string.Empty, "Error while user updated");
                return View(userItem);
            }
        }

        public IActionResult Remove(string id)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        public IActionResult SendEmail(string id)
        {
            try
            {
                var user = userService.GetUserById(id);


                var pass = PasswordGenerator.Create(8);
                user.PasswordHash = passwordHasher.HashPassword(user, pass);
                // send mail
                var emailMessage = new EmailMessage();
                emailMessage.To.Add(user.Email);
                emailMessage.Subject = "Welcome To Sysmon";
                emailMessage.Content = "Your password :" + pass;
                var emailResult = messagingService.SendMail(emailMessage);
                if (emailResult.Succeed)
                {
                    user.EmailConfirmed = true;
                    userService.Update(user);
                }

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}