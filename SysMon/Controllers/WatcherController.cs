﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SysMon.Models;
using SysMon.PageViewModels;
using SysMon.Services;
using SysMon.ViewModels;
using Microsoft.AspNetCore.Authorization;

namespace SysMon.Controllers
{
    [Authorize]
    public class WatcherController 
        : Controller
    {
        private readonly IWatcherService watcherService;
        private readonly IGroupService groupService;
        public WatcherController(
            IWatcherService watcherService,
            IGroupService groupService)
        {
            this.watcherService = watcherService;
            this.groupService = groupService;
        }

        public IActionResult Index()
        { 
            var watchers = watcherService.List();
            var groups = groupService.List();

            List<WatcherItem> watcherItems = new List<WatcherItem>();
            foreach (var watcher in watchers)
            {
                WatcherItem watcherItem = new WatcherItem();
                watcherItem.Id = watcher.Id;
                watcherItem.Email = watcher.Email;                
                watcherItem.IsWatcher = watcher.IsWatcher;
                watcherItem.ManagerEmail = watcher.ManagerEmail;
                watcherItem.ManagerName = watcher.ManagerName;
                watcherItem.ManagerPhone = watcher.ManagerPhone;
                watcherItem.Name = watcher.Name;
                watcherItem.PhoneNumber = watcher.PhoneNumber;

                watcherItem.GroupId = watcher.GroupId;
                var group = groups.SingleOrDefault(d => d.Id == watcher.GroupId);
                if(group != null)
                {
                    watcherItem.GroupName = group != null ? group.GroupName : String.Empty;
                }               

                watcherItems.Add(watcherItem);
            }

            return View(watcherItems);
        }

        public IActionResult Create()
        {
            var groups = groupService.List().Select(d => new KeyValue()
            {
                Key = d.Id.ToString(),
                Value = d.GroupName
            });
            ViewBag.ListOfGroup = groups;

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(WatcherItem watcherItem)
        {
            try
            {
                var watcher = new Watcher();
                watcher.Id = watcherItem.Id;
                watcher.Email = watcherItem.Email;
                watcher.IsWatcher = watcherItem.IsWatcher;
                watcher.ManagerEmail = watcherItem.ManagerEmail;
                watcher.ManagerName = watcherItem.ManagerName;
                watcher.ManagerPhone = watcherItem.ManagerPhone;
                watcher.Name = watcherItem.Name;
                watcher.PhoneNumber = watcherItem.PhoneNumber;

                watcher.GroupId = watcherItem.GroupId;

                watcherService.Add(watcher);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        public IActionResult Edit(int id)
        {
            var watcher = watcherService.Get(id);

            var watcherItem = new WatcherItem();
            watcherItem.Id = watcher.Id;
            watcherItem.Email = watcher.Email;
            watcherItem.IsWatcher = watcher.IsWatcher;
            watcherItem.ManagerEmail = watcher.ManagerEmail;
            watcherItem.ManagerName = watcher.ManagerName;
            watcherItem.ManagerPhone = watcher.ManagerPhone;
            watcherItem.Name = watcher.Name;
            watcherItem.PhoneNumber = watcher.PhoneNumber;
            watcherItem.GroupId = watcher.GroupId;
            
            var groups = groupService.List().Select(d => new KeyValue()
            {
                Key = d.Id.ToString(),
                Value = d.GroupName
            });
            ViewBag.ListOfGroup = groups;

            return View(watcherItem);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, WatcherItem watcherItem)
        {
            try
            {
                var watcher = watcherService.Get(id);
                watcher.Email = watcherItem.Email;
                watcher.Email = watcherItem.Email;
                watcher.IsWatcher = watcherItem.IsWatcher;
                watcher.ManagerEmail = watcherItem.ManagerEmail;
                watcher.ManagerName = watcherItem.ManagerName;
                watcher.ManagerPhone = watcherItem.ManagerPhone;
                watcher.Name = watcherItem.Name;
                watcher.PhoneNumber = watcherItem.PhoneNumber;
                watcher.GroupId = watcherItem.GroupId;

                watcherService.Update(watcher);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        public IActionResult Remove(int id)
        {
            try
            {
                watcherService.Remove(id);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}