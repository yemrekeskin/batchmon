﻿using SysMon.Services;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SysMon
{
    public static class DependencyProfile
    {
        public static void DependencyLoad(this IServiceCollection services)
        {
            services.AddScoped<IDbContext, ApplicationDbContext>();

            // socket
            services.AddSingleton<WebSocketHandler, NotificationHandler>();
            services.AddSingleton<WebSocketConnectionManager>();

            // messaging
            services.AddTransient<IEmailSender, EmailSender>();
            services.AddTransient<ISmsSender, SmsSender>();
            services.AddTransient<IMessagingService, MessagingService>();

            // repository and services             
            services.AddTransient<IGroupRepository, GroupRepository>();
            services.AddTransient<IGroupService, GroupService>();

            services.AddTransient<IWatcherRepository, WatcherRepository>();
            services.AddTransient<IWatcherService, WatcherService>();

            services.AddScoped<IJobRepository, JobRepository>();
            services.AddScoped<IJobService, JobService>();

            services.AddTransient<IJobConfigRepository, JobConfigRepository>();
            services.AddTransient<IJobConfigService, JobConfigService>();

            services.AddTransient<IJobNotificationRepository, JobNotificationRepository>();
            services.AddTransient<IJobNotificationService, JobNotificationService>();

            services.AddTransient<IJobScheduleRepository, JobScheduleRepository>();
            services.AddTransient<IJobScheduleService, JobScheduleService>();

            services.AddTransient<IConfigRepository, ConfigRepository>();
            services.AddTransient<IConfigService, ConfigService>();

            services.AddTransient<IRoleService, RoleService>();
            services.AddTransient<IUserService, UserService>();

            // scheduler
            services.AddScoped<INotificationService, NotificationService>();

            // Add scheduled tasks
            services.AddScoped<IScheduledTask, NotificationTask>();
            //services.AddTransient<IScheduledTask, TestTask>();

        }
    }
}
