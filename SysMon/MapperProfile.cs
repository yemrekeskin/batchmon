﻿using AutoMapper;
using SysMon.Models;
using SysMon.ServiceViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SysMon
{
    public class MapperProfile
        : Profile
    {
        public MapperProfile()
        {
            CreateMap<GroupViewModel, Group>();
            CreateMap<Group, GroupViewModel>();

            CreateMap<WatcherViewModel, Watcher>();
            CreateMap<Watcher, WatcherViewModel>();

            CreateMap<RoleViewModel, Role>();
            CreateMap<Role, RoleViewModel>();

            CreateMap<UserViewModel, User>();
            CreateMap<User, UserViewModel>();

            CreateMap<JobViewModel, Job>();
            CreateMap<Job, JobViewModel>();

            CreateMap<SearchJobViewModel, Job>();
            CreateMap<Job, SearchJobViewModel>();

            CreateMap<JobConfigViewModel, JobConfig>();
            CreateMap<JobConfig, JobConfigViewModel>();

            CreateMap<JobScheduleViewModel, JobSchedule>();
            CreateMap<JobSchedule, JobScheduleViewModel>();

            CreateMap<JobNotificationViewModel, JobNotification>();
            CreateMap<JobNotification, JobNotificationViewModel>();
        }
    }
}
