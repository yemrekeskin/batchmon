﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SysMon.Migrations
{
    public partial class RoleUpdated : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Explanation",
                table: "AspNetRoles",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Explanation",
                table: "AspNetRoles");
        }
    }
}
