﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SysMon.Migrations
{
    public partial class AddedSound : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Sound",
                table: "AspNetUsers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Sound",
                table: "AspNetUsers");
        }
    }
}
