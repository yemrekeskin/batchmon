﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SysMon.Migrations
{
    public partial class AddedSoundNOtf : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Sound",
                table: "JobNotifications",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Sound",
                table: "JobNotifications");
        }
    }
}
