﻿using SysMon.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SysMon.ViewModels
{
    public class BaseViewModel
    {
        public int Id { get; set; }
    }
}
