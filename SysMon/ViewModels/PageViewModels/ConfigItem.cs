﻿using SysMon.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SysMon.PageViewModels
{
    public class ConfigItem
        : BaseViewModel
    {
        public string SmtpServer { get; set; }
        public string SmtpPort { get; set; }
        public string Subject { get; set; }
        public string SmtpUserName { get; set; }
        public string SmtpPassword { get; set; }

        public int AlertRepetition { get; set; }
        public int AlertRepetitionRange { get; set; }
    }
}
