﻿using SysMon.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SysMon.PageViewModels
{
    public class GroupItem
        : BaseViewModel
    {
        public string GroupName { get; set; }
        public string Explanation { get; set; }
    }
}
