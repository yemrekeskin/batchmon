﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SysMon.PageViewModels
{
    public class JobConfigItem
    {
        public int Id { get; set; }

        public int JobId { get; set; } 
        public string JobName { get; set; }
        public string JobDetail { get; set; }

        public bool NotifyOnFailure { get; set; }
        public bool NotifyOnDelay { get; set; }
        public int ThresholdForDelay { get; set; }
        public bool Hot { get; set; }
    }
}
