﻿using SysMon.Models;
using SysMon.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SysMon.PageViewModels
{
    public class JobItem
        : BaseViewModel
    {
        public int JobRunId { get; set; }
        public string JobName { get; set; }
        public string JobDetail { get; set; }
        public string JobState { get; set; }
        public int Delay { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string ProjectName { get; set; }

        public bool IsOwned { get; set; } 
    }
}
