﻿using SysMon.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SysMon.PageViewModels
{
    public class UserItem
    {
        public string Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Surname { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }


        [Required]
        public string UserName { get; set; }

        public string Password { get; set; }

        [Required]
        public int GroupId { get; set; }
        public string GroupName { get; set; }

        [Required]
        public string RoleId { get; set; }
        public string RoleName { get; set; }
    }
}
