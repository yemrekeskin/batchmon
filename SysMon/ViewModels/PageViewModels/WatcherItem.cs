﻿using SysMon.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SysMon.PageViewModels
{
    public class WatcherItem
        : BaseViewModel
    {
        public int GroupId { get; set; }
        public string GroupName { get; set; } 

        [Required]
        public string Name { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        public string PhoneNumber { get; set; }

        [Required]
        public string ManagerName { get; set; }
        [Required]
        [EmailAddress]
        public string ManagerEmail { get; set; }
        [Required]
        public string ManagerPhone { get; set; }

        public bool IsWatcher { get; set; }
    }
}
