﻿using SysMon.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SysMon.ServiceViewModels
{
    public class AuthViewModel
    {
        public string Email { get; set; } 
        public string UserName { get; set; }
        public string Password { get; set; }

        public string DeviceId { get; set; }
        public DeviceType DeviceType { get; set; }
    }
}
