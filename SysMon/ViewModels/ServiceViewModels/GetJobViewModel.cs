﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SysMon.ServiceViewModels
{
    public class GetJobViewModel
    {
        public List<JobScheduleViewModel> Schedules { get; set; }  
        public List<JobViewModel> Jobs { get; set; }
    }
}
