﻿using SysMon.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SysMon.ServiceViewModels
{
    public class JobConfigViewModel
        : BaseViewModel
    {
        public string UserId { get; set; }
        public int JobId { get; set; }

        public bool NotifyOnFailure { get; set; }
        public bool NotifyOnDelay { get; set; }
        public int ThresholdForDelay { get; set; }
        public bool Hot { get; set; }
        public int JobRunId { get; set; }
    }
}
