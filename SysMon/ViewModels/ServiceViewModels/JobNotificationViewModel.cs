﻿using SysMon.Models;
using SysMon.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SysMon.ServiceViewModels
{
    public class JobNotificationViewModel
        : BaseViewModel
    {
        public long UserId { get; set; }
        public int JobId { get; set; }

        public string Title { get; set; }
        public string Message { get; set; }

        public NotifyState NotifyState { get; set; }
        public DateTime LastSentDate { get; set; }

        public int Counter { get; set; }

        public int JobRunId { get; set; }
    }
}
