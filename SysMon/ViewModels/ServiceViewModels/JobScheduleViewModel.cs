﻿using SysMon.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SysMon.ServiceViewModels
{
    public class JobScheduleViewModel
        : BaseViewModel
    {
        public DateTime DateTime { get; set; }
    }
}
