﻿using SysMon.Models;
using SysMon.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SysMon.ServiceViewModels
{
    public class JobViewModel
        : BaseViewModel
    {
        public int JobId { get; set; }
        public string JobName { get; set; }
        public string JobDetail { get; set; }
        public JobState JobState { get; set; }
        public int Delay { get; set; } 
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string ProjectName { get; set; }

        public bool NotifyOnFailure { get; set; }
        public bool NotifyOnDelay { get; set; }
        public int ThresholdForDelay { get; set; }
        public bool Hot { get; set; }
        public int JobRunId { get; set; }
        public int JobConfigId { get; set; } 
    }
}
