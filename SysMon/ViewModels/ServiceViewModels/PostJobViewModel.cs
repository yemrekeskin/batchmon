﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SysMon.ServiceViewModels
{
    public class PostJobViewModel
    {
        public List<JobScheduleViewModel> Schedules { get; set; }
        public List<JobConfigViewModel> JobConfigs { get; set; } 
    }
}
