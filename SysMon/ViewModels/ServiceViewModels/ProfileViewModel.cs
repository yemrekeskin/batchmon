﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SysMon.ServiceViewModels
{
    public class ProfileViewModel
    {
        public string RoleName { get; set; }
        public string GroupName { get; set; }

        public string Name { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }

        public string Token { get; set; }
        public string DeviceId { get; set; }

    }
}
