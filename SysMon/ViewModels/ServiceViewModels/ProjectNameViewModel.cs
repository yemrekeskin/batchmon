﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SysMon.ServiceViewModels
{
    public class ProjectNameViewModel
    {
        public string Name { get; set; }
    }
}
