﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SysMon.ServiceViewModels
{
    public class RoleViewModel
    {
        public int Id { get; set; } 
        public string Name { get; set; }
        public string Explanation { get; set; }

    }
}
