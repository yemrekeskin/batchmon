﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SysMon.ServiceViewModels
{
    public class UserProfileViewModel
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Password { get; set; }

        public bool IsManager { get; set; }
        public long ManagerUserId { get; set; }
    }
}
