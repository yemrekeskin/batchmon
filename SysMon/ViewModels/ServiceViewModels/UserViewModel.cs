﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SysMon.ServiceViewModels
{
    public class UserViewModel
    {
        public int Id { get; set; } 

        public string RoleId { get; set; }        
        public string RoleName { get; set; }

        public string GroupName { get; set; }

        public string Name { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Token { get; set; }

    }
}
