﻿using SysMon.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SysMon.ServiceViewModels
{
    public class WatcherViewModel
        : BaseViewModel
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }

        public string ManagerName { get; set; }
        public string ManagerEmail { get; set; }
        public string ManagerPhone { get; set; }

        public int GroupId { get; set; }
        public string GroupName { get; set; } 
        public bool IsWatcher { get; set; } 
    }
}
